\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{turnstile}
\usepackage{fancyvrb}
\usepackage{manfnt}

\input{Macros/macros.tex}
\DefineShortVerb{|}

\title{Formale Techniken der Software-Entwicklung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}

\begin{frame}
  \frametitle{Programmieren und Logik in ACL2}
  \begin{itemize}
  \item ACL2 ist eine Teilmenge von Common Lisp
  \item In Common Lisp sind manche Operationen nicht definiert
    (z.B. Addition von Strings)
  \item ACL2 definiert eine Vervollständigung aller Funktionen
  \item Das macht eine Übersetzung in Lisp ineffizient
  \item \emph{Guards} stellen sicher, dass Funktionen nur so verwendet
    werden, wie es in Common Lisp erlaubt ist
  \item Dadurch entstehen zusätzliche Beweisverpflichtungen
  \item \textbf{Die ACL2-Logik wird von Guards nicht beeinflusst}
  \end{itemize}
\end{frame}

\begin{frame}{Der ACL2 Theorembeweiser}
  \begin{itemize}
  \item Der Theorembeweiser versucht das jeweils letzte vom Benutzer
    erhaltene Theorem zu beweisen
  \item Dazu verwendet er Informationen, die er durch vorhergehende
    Theoreme erhalten hat
  \item Der Theorembeweiser verwendet zahlreiche Heuristiken um einen
    Beweis zu finden
  \item Der Benutzer hat nur eingeschränkte direkte Kontrolle über den
    Beweiser (durch "'Hinweise"` (Hints))
  \item Der Benutzer steuert den Beweiser hauptsächlich durch die
    geschickte Auswahl von Hilfstheoremen (Lemmata), die die
    Heuristiken des Beweisers veranlassen "`gute"' Beweisversuche zu
    unternehmen
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Definitionen}
  \begin{Acl2}[gobble=4]
    (defun $f$ ($x_1 \dots x_n$) 
      \verbit{body})
  \end{Acl2}
  \begin{itemize}
  \item Führt ein neues Funktionssymbol in die Logik ein
  \item Führt das Definitionsaxiom von $f$ ein:

    \begin{Acl2}
      ($f$ $x_1 \dots x_n$) $=$ \verbit{body}
    \end{Acl2}

  \item Das Definitionsaxiom wird vom Theorembeweiser als
    Termersetzungsregel übernommen: Ein Term der Form

    \begin{Acl2}
      ($f$ $a_1 \dots a_n$)
    \end{Acl2}

    wird durch

    \begin{Acl2}
      \verbit{body}$[x_1 \mapsto a_1, \dots, x_n \mapsto a_n]$
    \end{Acl2}

    ersetzt
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Theoreme}
  \begin{Acl2}[gobble=4]
    (defthm $t$
      \verbit{formula}
      :rule-classes (\verbit{class}$_1$ \dots \verbit{class}$_n$))
  \end{Acl2}

  \begin{itemize}
  \item Startet einen Beweisversuch für $t$
  \item Führt (Termersetzungs)-Regeln entsprechend der
    \texttt{:rule-classes} ein, falls der Beweis erfolgreich war
  \end{itemize}  
\end{frame}


\begin{frame}{Struktur des Beweisers}
  \begin{center}
    \includegraphics[width=11cm]{Images/ACL2-Struktur}
  \end{center}
\end{frame}


\begin{frame}{Die Organisation des ACL2-Theorembeweisers}
  \begin{itemize}
  \item Pool von zu beweisenden Formeln
    \begin{itemize}
    \item Anfänglich nur das zu beweisende Theorem
    \item Jeder Verarbeitungsschritt kann neue Formeln in den Pool
      legen, die noch bewiesen werden müssen
    \end{itemize}
  \item Sechs Beweistechniken
    \begin{itemize}
    \item \textbf{Simplifikation}
    \item Destruktorelimination
    \item Verwendung von Äquivalenzen
    \item Generalisierung
    \item Irrelevanzelimination
    \item \textbf{Induktion}
    \end{itemize}
  \item Die Beweistechniken werden in dieser Reihenfolge auf jede
    Formel aus dem Pool angewandt bis
    \begin{itemize}
    \item Keine Formel mehr im Pool ist (erfolgreicher Beweis)
    \item Keine Beweistechnik mehr eine Änderung im Pool bewirkt
    \end{itemize}
  \item Es kann auch vorkommen, dass der Beweiser in eine
    Endlosschleife gerät
  \end{itemize}
\end{frame}

\begin{frame}{Der Wasserfall}
  \begin{itemize}
  \item Die Abarbeitung der Beweistechniken wird manchmal als
    "`Wasserfall"' bezeichnet
  \item Eingebe für jede Beweistechnik $B$ des Wasserfalls ist eine
    Formel $\phi$
  \item Falls $B$ an $\phi$ eine Vereinfachung vornehmen kann, so ist
    das Ergebnis von $B$ eine Menge von Formeln
    $\Gamma = \{\psi_1, \dots \psi_n\}$ für die gilt
    \begin{displaymath}
      \psi_1 \land \cdots \land \psi_n \implies \phi
    \end{displaymath}
  \item Ist $n = 0$, also $\Gamma = \emptyset$, so hat $B$ die Formel
    $\phi$ bewiesen.
  \item Andernfalls werden die $\psi_i$ in den Pool gelegt und der
    Wasserfall wird neu gestartet
  \end{itemize}
\end{frame}

\begin{frame}{Der Wasserfall}
    \dbend\hspace*{2mm}
  \parbox[t]{0.9\textwidth}{\raggedright Eine Beweistechnik erzeugt
    Formeln $\psi_1, \dots, \psi_n$ mit 
    \begin{displaymath}
      \psi_1 \land \cdots \land \psi_n \implies \phi
    \end{displaymath}
    Die Aussage $\phi \implies \psi_1 \land \cdots \land \psi_n$ kann,
    muss aber nicht gelten.  Es kann also vorkommen, dass eine
    Beweistechnik $B$ falsche Formeln erzeugt und in den Pool legt.

    Das heißt nicht, dass der ACL2 Beweiser inkonsistent ist, sondern
    nur, dass er Heuristiken verwendet, die für manche wahre Formeln
    keinen Beweis finden.  (Das geht für die ACL2-Logik
    nicht anders, weil sie unentscheidbar ist.)  

    Wenn das passiert muss man den Beweiser mit zusätzlichen Eingaben
    zum richtigen Beweis steuern.}
\end{frame}

\begin{frame}{Steuerung des Beweisers}
  \begin{itemize}
  \item Der ACL2-Beweiser erlaubt keine interaktive Steuerung nachdem
    der Beweisversuch angefangen wurde
  \item Bereits bewiesene Theoreme werden von den Beweistechniken zur
    Vereinfachung hergenommen
  \item Dazu kann man mit dem Schlüsselwort \texttt{:rule-classes} in
    jeder \texttt{defthm}-Form angeben für welche Arten
    Beweistechniken das Theorem eingesetzt werden soll
  \item Standardwert für \texttt{:rule-classes} ist
    \texttt{(:rewrite)}\\
    Damit wird eine Termersetzungsregel für den Simplifikationsschritt
    erzeugt
  \item Um zu verhindern, dass eine Termersetzungsregel aus einem
    Theorem abgeleitet wird schreibt man \texttt{:rule-classes nil}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Simplifikation}
  Simplifikation ist (neben Induktion) die wichtigste Komponente des
  Theorembeweisers.  Der Simplifikationsschritt verfährt nach
  folgendem Schema
  \begin{itemize}
  \item Anwendung von Entscheidungsprozeduren für Aussagenlogik,
    Gleichheit und rationale lineare Arithmetik
  \item Anwendung von Typinformation und Forward-Chaining Regeln um
    für jeden Teilterm einen \emph{Kontext} zu erzeugen
  \item Rewriting jedes Teilterms in seinem Kontext unter Verwendung
    von
    \begin{itemize}
    \item Definitionen
    \item Bedingten Termersetzungsregeln
    \item Metafunktionen
    \item Propositionale Normalisierung
    \end{itemize}
    Termersetzung kann dabei beliebige Kongruenzrelationen
    berücksichtigen.
  \end{itemize}
\end{frame}

\begin{frame}{Entscheidungsprozeduren}
  \begin{itemize}
  \item Eine \emph{Entscheidungsprozedur} ist ein Verfahren, dass
    Probleme aus einer bestimmten Theorie automatisch lösen kann
  \item Das geht nur für entscheidbare Theorien wie
    \begin{itemize}
    \item Aussagenlogik
    \item Lineare Gleichungen und Ungleichungen
    \item Gleichheitslogik
    \end{itemize}
  \item Unentscheidbare Theorien können keine Entscheidungsprozeduren
    haben
    \begin{itemize}
    \item Prädikatenlogik
    \item Terminierung rekursiver Programme
    \item Diophantische Gleichungen
    \end{itemize}
  \item Für die komplette ACL2-Logik gibt es also keine
    Entscheidungsprozedur, man muss Heuristiken zur Suche nach
    Beweisen verwenden
  \item Für Aussagenlogik, rationale lineare Ungleichungen und
    Kongruenzabschluss hat ACL2 eingebaute Entscheidungsprozeduren
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Beispiel: Entscheidungsprozeduren}
  ACL2 kann die folgende Aussagen automatisch zeigen:
  \begin{itemize}
  \item Lineare Rationale Ungleichungen
  \begin{Acl2}
(thm
 (implies (and (< (* 3 a) (* 2 b))
               (<= (* 5 b) (+ (* 7 a) c)))
          (< a (* 2 c))))
  \end{Acl2}
\item Aussagenlogik
  \begin{Acl2}
(thm
 (implies (and (implies a b) (implies b c))
          (implies (and a b) c)))
  \end{Acl2}
  \end{itemize}
\end{frame}

\begin{frame}{Typen in ACL2}
  In ACL2 gibt es die folgenden 14 Basistypen
  \begin{itemize}
  \item $\{0\}$
  \item Die positiven ganzen Zahlen; die positiven (echten) Brüche
  \item Die negativen ganzen Zahlen; die negativen (echten) Brüche
  \item Die echt komplexen rationalen Zahlen
  \item Zeichen; Strings
  \item $\{\mathtt{t}\}$, $\{nil\}$
  \item Die Symbole außer $\{\mathtt{t}\}$ und $\{nil\}$
  \item Echte Cons-Zellen (d.h., geschachtelte Cons-Zellen, deren
    letzter \texttt{cdr} den Wert \texttt{nil} hat)
  \item Unechte ("`punktierte"') Cons-Zellen
  \item Alle anderen Elemente des ACL2-Universums
  \end{itemize}
\end{frame}

\begin{frame}{Typen in ACL2}
  \begin{itemize}
  \item Ein \emph{Typ} ist eine Vereinigung aus Basistypen
  \item Z.B.: Die natürlichen Zahlen sind die Vereinigung aus
    $\{\texttt{nil}\}$ und den positiven ganzen Zahlen
  \item Typen werden durch Terme, die aus folgenden Bestandteilen
    gebildet werden repräsentiert:
    \begin{itemize}
    \item \texttt{0}, \texttt{<}
    \item \texttt{integerp}, \texttt{rationalp},
      \texttt{complex-rationalp}, \texttt{acl2-numberp}
    \item \texttt{characterp}, \texttt{stringp}
    \item \texttt{equal}, \texttt{null}, \texttt{nil}, \texttt{t}
    \item \texttt{symbolp}
    \item \texttt{consp}, \texttt{atom}, \texttt{listp}, \texttt{true-listp}
    \end{itemize}
  \item Makros, die in diese Terme expandieren sind ebenfalls zulässig
  \item Aus diesen Termen kann man Vereinigung, Durchschnitt und
    Komplement berechnen: \texttt{or}, \texttt{and}, \texttt{not}
  \end{itemize}
\end{frame}

\begin{frame}{Typen in ACL2}
  Um die Typanalyse zu verbessern kann man zwei Arten von Regeln
  einführen
  \begin{itemize}
  \item Typ-Vorschriften ("`Type Prescription"'), die den Ergebnistyp
    einer Funktion beschreiben
  \item Compound Recognizers, die für Boole'sche Funktionen mit einem
    Argument definiert werden können.  Compound Recognizer bestimmen,
    wie Typinformation über das Argument gewonnen werden kann
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Kontext}
  Termersetzung wird in einem \emph{Kontext} vorgenommen, der folgende
  Information enthält:
  \begin{itemize}
  \item Eine "`lineare Datenbank"' für die Entscheidungsprozedur der
    linear-rationalen Ungleichungen
  \item Typaussagen, die Typen für bestimmte Ausdrücke festlegen
  \end{itemize}

  Der Kontext für die Konklusion wird aus den Hypothesen gebildet.
  Der Kontext für Hypothesen wird gebildet aus
  \begin{itemize}
  \item Den anderen Hypothesen
  \item Der Negation der Konklusion
  \end{itemize}
  Bei der Konstruktion des Kontexts wird mittels
  \emph{Forward-Chaining} der Abschluss aus den zur Verfügung
  stehenden Termen gebildet.
\end{frame}

\begin{frame}{Äquivalenzrelationen}
  Eine Relation zwischen Elementen aus Mengen $A_1, \dots, A_n$ ist
  eine Teilmenge des kartesischen Produkts $\prod_{i=1}^n A_i$.
  \begin{definition}[Äquivalenzrelation]
    Eine Äquivalenzrelation $\sim$ ist eine reflexive, symmetrische,
    transitive binäre Relation:
    \begin{itemize}
    \item Reflexivität: $x \sim x$
    \item Symmetrie: $x \sim y \implies y \sim x$
    \item Transitivität: $x \sim y \land y \sim z \implies x \sim z$
    \end{itemize}
  \end{definition}
\end{frame}

\begin{frame}[fragile]{Äquivalenzrelationen}
  Die folgende Funktion
  \begin{Acl2}
(defun set-equiv (x y)
  (and (subsetp-equal x y)
       (subsetp-equal y x)))    
  \end{Acl2}
  Ist eine Äquivalenzrelation:
  \begin{Acl2}
(defthm set-equiv-is-an-equivalence
  (and (booleanp (set-equiv x y))
       (set-equiv x x)
       (implies (set-equiv x y)
                (set-equiv y x))
       (implies (and (set-equiv x y) (set-equiv y z))
                (set-equiv x z)))
  :rule-classes (:equivalence))
  \end{Acl2}
\end{frame}

\begin{frame}[fragile]{Äquivalenzrelationen}
  Kürzere Schreibweise für Äquivalenzrelationen:
  \begin{Acl2}
    (defequiv set-equiv)
  \end{Acl2}

  Der Makro \texttt{defequiv} expandiert in die auf der letzten Folie
  gezeigte Form:
  \begin{Acl2}
(defthm set-equiv-is-an-equivalence
  (and (booleanp (set-equiv x y))
       (set-equiv x x)
       (implies (set-equiv x y)
                (set-equiv y x))
       (implies (and (set-equiv x y) (set-equiv y z))
                (set-equiv x z)))
  :rule-classes (:equivalence))
  \end{Acl2}
\end{frame}

\begin{frame}[fragile]{Verfeinerung}
  Man sagt, eine Äquivalenzrelation $\sim_1$
  \emph{verfeinert} eine Äquivalenzrelation $\sim_2$, wenn
  gilt $x \sim_1 y \implies x \sim_2 y$. 

  In ACL2 lässt sich das durch \texttt{:refinement} Regeln ausdrücken:
  \begin{Acl2}
(defthm list-equiv-refines-set-equiv
  (implies (list-equiv x y)
           (set-equiv x y))
  :rule-classes (:refinement))    
  \end{Acl2}

  Mit dem Makro \texttt{defrefinement} kann man das wieder abkürzen:
  \begin{Acl2}
(defrefinement list-equiv set-equiv)
  \end{Acl2}
\end{frame}

\begin{frame}[fragile]{Kongruenzrelationen}
  \begin{definition}[Kongruenzrelation]
    Seien $\mathit{equiv}_1$ und $\mathit{equiv}_2$ zwei
    Äquivalenzrelationen.  Wir sagen $\mathit{equiv}_1$ ist eine
    Kongruenzrelation bezüglich $\mathit{equiv}_2$ und (dem $i$-ten
    Argument von) $\mathit{f}$, wenn gilt
    \begin{Acl2}
      (implies ($\mathit{equiv}_1$ x$_i$ y$_i$)
               ($\mathit{equiv}_2$ ($\mathit{f}$ x$_1$ \dots x$_i$ $\dots$)
                      ($\mathit{f}$ x$_1$ \dots y$_i$ $\dots$)))
    \end{Acl2}
    Dafür sagt man auch, $\mathit{equiv}_1$-Substitution im $i$-ten
    Argument von $\mathit{f}$ sei unter Erhaltung der
    $\mathit{equiv}_2$-Äquivalenz möglich.
  \end{definition}
\end{frame}

\begin{frame}[fragile]{Kongruenzrelationen in ACL2}
  In ACL2 lassen sich Kongruenzrelationen durch Implikationen der
  beschriebenen Form mit Regelklasse \texttt{:congruence} einführen:
  \begin{Acl2}
(defthm set-equiv-implies-iff-member-2
  (implies (set-equiv y y-equiv)
           (iff (member x y) (member x y-equiv)))
  :rule-classes (:congruence))    
  \end{Acl2}
  Kürzer kann man das wieder mit dem Makro \texttt{defcong} schreiben
  \begin{Acl2}
    (defcong set-equiv iff (member x y) 2)
  \end{Acl2}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Termersetzung}
  \begin{itemize}
  \item Termersetzungsregeln werden von Axiomen, Definitionen und
    erfolgreichen Beweisen generiert
  \item Einfache (unbedingte) Termersetzungsregeln ersetzen einen
    Subterm durch einen anderen
  \item Beispiel: Das Axiom
    $\texttt{(equal (car (cons x y)) x)} \neq \nil$ führt zur
    unbedingten Termersetzungsregel
    \begin{Acl2}
       \,(car (cons x y)) $\to$ x
    \end{Acl2}
    \bigskip
    Beispiel:
    \begin{displaymath}
      \texttt{(+ 1 (car (cons 3 nil)))} \to\texttt{ (+ 1 3)}
    \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Bedingte Termersetzung}
  \begin{itemize}
  \item Bedingte Termersetzungsregeln ersetzen einen Subterm durch
    einen anderen, wenn eine Bedingung gezeigt werden kann
  \item Beispiel: Das Axiom
    \begin{Acl2}
      (implies (not (consp x)) (equal (car x) nil))
    \end{Acl2}
    führt zur folgenden
    
    \bigskip
    \begin{block}{Termersetzungsregel}
      Falls \texttt{(not (consp x))} aus dem Kontext gezeigt werden
      kann, dann ersetze

      \begin{Acl2}
        (car x) $\to$ nil
      \end{Acl2}
    \end{block}
  \end{itemize}
\end{frame}

\begin{frame}{Bedingte Termersetzung (Beispiel)}
  Mit der Regel der letzten Folie kann man ersetzen:
    \begin{displaymath}
      \texttt{(cons 1 (car 2))} \to \texttt{(cons 1 nil)}
    \end{displaymath}
    da \texttt{(not (consp 2))} in jedem Kontext gilt

    \bigskip
    Man kann ebenso ersetzen
    \begin{multline*}
      \texttt{(and (natp x) (equal y (cons 1 (car x))))}\\ \to\\ 
       \texttt{(and (natp  x) (equal y (cons 1 nil)))}
    \end{multline*}
    da \texttt{(natp x)} zum Kontext von \texttt{(equal y
      (cons 1 (car x)))} gehört und aus \texttt{(natp x)} folgt
    \texttt{(not (consp x))}
\end{frame}

\begin{frame}
  \frametitle{(Bedingte) Termersetzung}
  \begin{itemize}
  \item Die Anwendung von Termersetzungen wird durch Heuristiken
    gesteuert
    \begin{itemize}
    \item Öffnen von Funktionsdefinitionen
    \item Kommutative Regeln (allgemeiner: permutative Regeln)
    \end{itemize}

  \item Um die Hypothesen einer bedingten Termersetzung zu zeigen
    wird Termersetzung und propositionale Vereinfachung angewandt
  \item Die rekursive Anwendung der logischen und Termersetzungsregeln
    nennt man \emph{Backchaining}
  \item Backchaining führt leicht zu unendlichen Reduktionen
  \item Dieses Risiko wird durch Heuristiken von ACL2 vermindert aber
    nicht eliminiert
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Termersetzung und Kongruenzen}
  \begin{itemize}
  \item Termersetzung kann nicht nur gleiche Terme (bzgl.\
    \texttt{equal}) ersetzen, sondern kongruente Terme
  \item Ein Term \textit{lhs} heißt $\mathit{equiv}$-hittable
    (bezüglich einer Kongruenzrelation $\mathit{equiv}$), wenn
    Kongruenzregeln existieren, die die Ersetzung von \textit{lhs}
    durch einen äquivalenten Term \texttt{rhs} rechtfertigen
  \item Termersetzung modulo Kongruenzen ist wichtig um mit
    Datentypen, die als Listen repräsentiert sind umgehen zu können.
    Für diese Datentypen ist oft \texttt{equal} zu fein
  \item Beispiel: \texttt{set-equiv}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Destruktorelimination}
  \begin{itemize}
  \item Destruktorenelimination ersetzt manche Vorkommen von Termen,
    auf die Zugriffsfunktionen (Destruktoren) angewandt werden durch
    eine explizite Konstruktion des Terms, so dass Variablen für die
    Destruktorenanwendungen eingesetzt werden können
  \item Beispiel: Ersetze
\begin{Acl2}
  (implies (consp \blue{x})
           (and (equal y \blue{(car x)})
                (equal z \blue{(cdr x)})))
\end{Acl2}
    durch
\begin{Acl2}
  (implies (consp \red{(cons a b)})
           (and (equal y \red{a})
                (equal z \red{b})))
\end{Acl2}

(Das kann weiter zu \texttt{(and (equal y a) (equal z b))} vereinfacht
werden)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Verwendung von Äquivalenzen}
  \begin{itemize}
  \item Enthält eine Formel ein Vorkommen von \texttt{(equal
    }\textit{lhs} \textit{rhs}\texttt{)} und weitere Vorkommen von
    \textit{lhs}, so werden diese durch \textit{rhs} ersetzt (oder
    umgekehrt)
  \item Ausnahme: In Subtermen der Form \texttt{(equal }\textit{x}
    \textit{y}\texttt{)} werden manchmal nur Vorkommen auf einer Seite
    des Terms ersetzt \textit{(Cross-Fertilization)}
  \item Kommt die Formel \texttt{(equal }\textit{lhs}
    \textit{rhs}\texttt{)} aus einer Induktion so wird sie danach
    verworfen
  \item (Dadurch kann es vorkommen, dass die Eingabeformel eine
    Tautologie ist, die Ausgabe nicht!)
  \item Diese Ersetzungen werden nicht nur für \texttt{equals} sondern
    für beliebige Äquivalenzen durchgeführt
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Generalisierung}
  \begin{itemize}
  \item Subterme, die
    \begin{itemize}
    \item in Hypothese und Konklusion einer Implikation
    \item in zwei Hypothesen einer Implikation
    \item auf beiden Seiten einer Äquivalenzrelation
    \end{itemize}
    vorkommen werden durch eine Variable ersetzt
  \item Beispiel:

    \begin{Acl2}
      (implies (natp \blue{(+ x 1)})
               (integerp \blue{(+ x 1)}))
    \end{Acl2}

    wird ersetzt durch
    \begin{Acl2}
      (implies (natp \red{y}) (integerp \red{y}))
    \end{Acl2}

  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Irrelevanzeliminierung}
  \begin{itemize}
  \item Partitioniert Hypothesen in Cliquen bezüglich ihrer
    Variablenvorkommen.
  \item Kommt eine isolierte Clique vor, so ist diese entweder
    irrelevant, oder die Formel ist eine Tautologie (wenn die Terme
    der Clique widersprüchlich sind)
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{Wiederholung: Das Induktionsprinzip von ACL2}
  Eine Formel $\phi$ kann in der ACL2-Logik folgendermaßen bewiesen
  werden:
  \begin{itemize}
  \item Basisfall:\\
    \texttt{(implies (and (not }$q_1$\texttt{)}
      \dots\texttt{(not }$q_k$\texttt{))} $\phi$\texttt{)}
  \item Induktionsschritte:
    Für $1 \leq i \leq k$:\\
    \begin{Acl2}[gobble=6]
      (implies (and $q_i$
                    $\phi/\sigma_{i,1}$
                    \dots
                    $\phi/\sigma_{i,h_i}$)
               $\phi$)
    \end{Acl2}
  \end{itemize}
  \vspace*{-2mm}
  sofern für die Terme $m$, $q_1$, \dots $q_k$ und Substitutionen
  $\sigma_{i,j}$ mit $1 \leq i \leq k$ und $1 \leq j \leq h_i$ die
  folgenden Aussagen gelten:
  \begin{itemize}
  \item \texttt{(o-p }$m$\texttt{)}
  \item \texttt{(implies }$q_i$ \texttt{(o< }$m/\sigma_{i,j}\ m$\texttt{))}
  \end{itemize}
\end{frame}

\begin{frame}{Wahl des Induktionsschemas}
  Wo kommen die $q_i$ und die Substitutionen $\sigma_{i, h_i}$ auf der
  letzten Folie her?

  \begin{itemize}
  \item Das Induktionsprinzip ist \emph{korrekt} für beliebige Wahl
    der $q_i$ und $\sigma_{i, h_i}$ (sofern die angegebenen
    Bedingungen erfüllt sind)
  \item Aber die meisten Instantiierungen für die $q_i$ und
    $\sigma_{i, h_i}$ werden nicht zu einer erfolgreichen Induktion
    führen
  \item Analyse der syntaktischen Struktur der Funktionsdefinition
  \item Die $q_i$ werden, wie beim Maß für die Zulassung der Funktion,
    durch die beherrschenden Terme berechnet
  \item Die $\sigma_{i, h_i}$ sind die im von $q_i$ beherrschten Fall
    vorkommenden Argumente für die rekursiven Funktionsaufrufe.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Induktion}
  \begin{itemize}
  \item Das bei der Zulassung einer rekursiven Funktion gefundene Maß
    wird auch zur Rechtfertigung einer Induktion über diese Funktion
    verwendet werden
  \item Die rekursiven Aufrufe im Rumpf der Funktion beeinflussen die
    Struktur des Induktionsschemas ("`scheme suggested by\dots"')
  \item Manchmal wären auch andere Induktionsschemata sinnvoll
  \item Das kann man durch Angabe der Regelklasse \texttt{:induction}
    erreichen
  \end{itemize}
\end{frame}

\begin{frame}{Induktion}
  Wenn eine Aussage beim Induktions-Beweisschritt ankommt verfährt
  ACL2 folgendermaßen:
  \begin{itemize}
  \item Alle durch die Aussage vorgeschlagenen Induktionsschemata werden
    berechnet
  \item Die Schemata werden verglichen und evtl. kombiniert; daraufhin
    wird ein Schema ausgewählt
  \item Das ausgewählte Schema wird für die Formel instantiiert
  \item Das instantiierte Schema wird propositional vereinfacht;
    dadurch entstehen typischerweise mehrere Formeln
  \item Die Formeln werden zum Pool hinzugefügt
  \end{itemize}
\end{frame}

\begin{frame}{Induktion: Beispiel zur Vereinfachung}
  Beispiel: 
  \begin{itemize}
  \item Formel $\phi$: $\psi \implies \xi$
  \item Test $q$
  \item Induktionshypothese $\phi'$ (= $\phi\sigma$):
    $\psi' \implies \xi'$
  \end{itemize}
  Daraus ergibt sich das Induktionsschema
  \begin{displaymath}
    (q \land (\psi' \implies \xi')) \implies (\psi \implies \xi)
  \end{displaymath}
  Das wird propositional vereinfacht zu den zwei Formeln
  \begin{gather*}
    (q \land \lnot \psi' \land \psi) \implies \xi\\
    (q \land \xi' \land \psi) \implies \xi
  \end{gather*}
\end{frame}

\begin{frame}
  \frametitle{Die Methode}
  "`Die Methode"' ("`the Method"') ist ein Vorgehensmodell, wie man
  mit dem Theorembeweiser von ACL2 umgehen kann.  Die Methode wird von
  ACL2s unterstützt.  Bei der Methode unterteilt man den Editor in
  zwei Teile
  \begin{itemize}
  \item Die bereits bewiesenen Aussagen (in ACL2s grau hinterlegt)
  \item Die noch offenen Aussagen (TODO-Liste), in ACL2s weiß
    hinterlegt)
  \item Die (imaginäre) Trennlinie zwischen bewiesenen und offenen
    Aussagen nennt sich \emph{Barriere (Barrier)}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Die Methode}
  Um einen Beweis zu finden geht man folgendermaßen vor:  Schreibe die
  zu beweisende Aussage in die TODO-Liste.  Dann
  \begin{enumerate}
  \item Denke über den Beweis der ersten Aussage in der TODO-Liste
    nach.  Wird sie mit Induktion oder Simplifikation bewiesen?  Sind
    die notwendigen Lemmata bereits bewiesen?  Wenn ja, gehe zum
    nächsten Schritt.  Andernfalls füge die Lemmata zur TODO-Liste
    hinzu und gehe wieder zu 1.
  \item Rufen den Theorembeweiser für die erste Aussage in der
    TODO-Liste auf. Lasse den Beweis höchstens für ein paar Sekunden
    laufen.
  \item Wenn der Beweis erfolgreich war, verschiebe die Barriere über
    die gerade bewiesene Aussage und gehe zu Schritt 1.
  \item Analysiere die Ausgabe des Beweises (von Anfang an, nicht das
    Ende).  Suche Abweichungen von der Beweisidee aus 1. und finde
    heraus, wie man sie beseitigen kann.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Die Methode}
  \begin{itemize}
  \item Um fehlgeschlagene Beweise zu korrigieren verwendet man
    hauptsächlich zwei Techniken
    \begin{itemize}
    \item Hinzufügen von Lemmata mit den entsprechenden
      \texttt{rule-classes}.
    \item Hinzufügen von Hinweisen (Hints) zum aktuellen Theorem, wenn
      die notwendigen Lemmata schon zur Verfügung stehen aber vom
      Theorembeweiser nicht verwendet werden.  Oft ist das auf
      unerwünschte vorhergehende Reduktionen zurückzuführen.
    \end{itemize}
  \item Man kann Theoreme, die zu unerwünschten Reduktionen führen
    auch global deaktivieren (z.B. mit \texttt{defthmd}) oder ihnen
    keine Regelklasse zuordnen (\texttt{:rule-classes nil})
  \item Werden Aussagen nur für Teile eines Beweises verwendet kann
    man sie mit \texttt{encapsulate} kapseln.
  \item Größere Beweise bzw. Theorien kann man in wiederverwendbare
    \emph{Bücher} aufteilen.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiele}
  Die folgenden Beispiele sind alle im Ordner \texttt{Lecture-05} im
  GitLab Repository:
  \begin{itemize}
  \item \texttt{simple-examples.lisp}
  \item \texttt{fact.lisp}
  \item \texttt{ac-example.lisp}
  \item \texttt{insertion-sort.lisp}
  \item \texttt{tree.lisp}
  \item \texttt{adder.lisp}
  \end{itemize}
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
