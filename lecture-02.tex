\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{synttree}
\usepackage{fancyvrb}
\usepackage{manfnt}

\input{Macros/macros.tex}

\title{Formale Techniken der Software-Entwicklung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}

\section{Logik}\label{logik}

\begin{frame}{Logik}
  Verfahren um aus gegebenen Aussagen gültige/wahre Schlüsse zu ziehen
\end{frame}

\begin{frame}{Vorteile}
  \begin{itemize}
  \item Logik kann exakte Bedeutung von Begriffen und Aussagen
    festlegen
  \item Logische Schlüsse sind risikofrei
  \item Beweise
    \begin{itemize}
    \item dass Spezifikation gewisse Eigenschaften hat
    \item dass Implementierung der Spezifikation genügt
    \end{itemize}
  \item Automatisierung
  \end{itemize}
\end{frame}

\begin{frame}{Nachteile}
  \begin{itemize}
  \item Oft sind Begriffe nicht exakt definierbar
  \item Oft sind Aussagen unsicher oder nur ``fast wahr''
    \begin{itemize}
    \item Frame Problem
    \end{itemize}
  \item Riskante Schlüsse sind oft notwendig
  \item Logisch spezifizierte Eigenschaften unterscheiden sich oft von
    den erwarteten/gewünschten Eigenschaften
  \item Komplexität
    \begin{itemize}
    \item der Spezifikation
    \item des Beweisens
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Verschiedene Logiken}

  \begin{itemize}
  \item Quantoren

    \begin{itemize}
    \item Aussagenlogik
    \item Quantorenfreie Logik erster Stufe mit Induktion (ACL2)
    \item Logik erster Stufe (Prädikatenlogik)
    \item Logik höherer Stufe (Typtheorie)
    \end{itemize}
  \item Konstruktivität

    \begin{itemize}
    \item Intuitionistische Logik
    \item Klassische Logik
    \end{itemize}
  \item Modalitäten

    \begin{itemize}
    \item Klassische Logik (ohne Modalitäten)
    \item Temporallogik
    \item Modallogik
    \end{itemize}
  \end{itemize}

\end{frame}

\section{Aussagenlogik}\label{aussagenlogik}

\begin{frame}{Aussagenlogik}

  \begin{itemize}
  \item Beschäftigt sich mit deklarativen Sätzen (Aussagen) und
    Schlussfolgerungen, die aus (Mengen von) Aussagen getroffen werden
    können
  \item Einfache Aussagen werden durch \emph{Aussagenvariablen}
    (propositionale Variablen) repräsentiert
  \item Zusammengesetzte Aussagen werden durch \emph{Junktoren} wie
    ``und'' oder ``nicht'' aus einfachen Aussagen gebildet.
  \end{itemize}

\end{frame}

\begin{frame}{Aussagen}

  \begin{itemize}
  \item Sätze, die prinzipiell mit ``Ja'' oder ``Nein'' beantwortet
    werden können
  \item Der Inhalt von Aussagen wird nicht betrachtet, nur ihr
    Wahrheitswert
  \item Die Struktur von einfachen Aussagen wird nicht betrachtet

    \begin{itemize}
    \item Aussagen nicht über Objekte parametrisierbar
      \begin{itemize}
      \item Keine Allaussagen
      \item Keine Existenzaussagen
      \end{itemize}
    \item Keine Modalitäten
      \begin{itemize}
      \item "`notwendigerweise"'
      \item "`möglicherweise"'
      \item "`sollte"' 
      \item "`in der Zukunft"'
      \end{itemize}

    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}{Einfache Aussagen}

  \begin{itemize}
  \item 2 + 2 = 4
  \item 2 + 2 = 5
  \item Kein Ablauf des Systems führt zu einem Deadlock
  \item Hans war gestern betrunken
  \item Alle Menschen sind sterblich
  \item Plato war ein Mensch
  \item Alle Marsmenschen mögen Pizza
  \end{itemize}

\end{frame}

\begin{frame}{Zusammengesetzte Aussagen}

  \begin{itemize}
  \item ``2 + 2 = 4'' oder ``2 + 2 = 5''
  \item ``Alle Menschen sind sterblich'' und ``Plato war ein Mensch''
  \item Wenn kein Ablauf des Systems zu einem Deadlock führt, dann
    reagiert das System immer auf Eingaben

    \begin{itemize}
    \item Genauer ``Kein Ablauf des Systems führt zu einem Deadlock''
      impliziert ``Das System reagiert immer auf Eingaben''
    \end{itemize}
  \item Wenn der Mond aus grünem Käse ist, dann mögen alle Marmenschen
    Pizza

    \begin{itemize}
    \item Genauer: ``Der Mond ist aus grünem Käse'' impliziert ``Alle
      Marsmenschen mögen Pizza''
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}{Keine Aussagen}

  \begin{itemize}
  \item Wie geht es dir heute?
  \item Komm sofort hierher!
  \item Wozu soll diese Vorlesung denn gut sein?
  \end{itemize}

\end{frame}

\begin{frame}{Deduktion (Schlussfolgerungen)}

  \begin{itemize}
  \item Wenn kein Ablauf des Systems zu einem Deadlock führt, dann
    reagiert das System immer auf Eingaben
  \item Kein Ablauf des Systems führt zu einem Deadlock
  \item Daraus folgt: Das System reagiert immer auf Eingaben
  \end{itemize}

\end{frame}

\section{Syntax}\label{syntax}

\begin{frame}{Syntax}

  \begin{itemize}
  \item Menge von Aussagenvariablen $\A = \{A, B, C, \dots\}$
  \item Konstantensymbole: $\top$, $\bot$
  \item Logische Symbole (Junktoren): $\lnot, \land, \lor, \implies,
    \iff, \xor$
  \item Hilfssymbole: $(,)$
  \item Ausagenlogische Formeln (Terme) werden rekursiv aus Variablen,
    Junktoren und Hilfssymbolen gebildet
  \end{itemize}

  Aussagenvariablen (propositionale Variablen)

  \begin{itemize}
  \item Stehen für ganze Aussagen
  \item Können nur die Werte $\true$ oder $\false$ annehmen
  \end{itemize}

\end{frame}

\begin{frame}{Aussagenlogische Formeln}
  Sei $\A$ eine Menge aussagenlogischer Variablen.
  \begin{displaymath}
\begin{array}{rcll}
    \qquad
    \F & = & \A          & \text{Variable}\\
    &|& \top             & \text{Top, Verum, wahr}\\
    &|& \bot             & \text{Bottom, Falsum, falsch}\\
    &|& (\lnot \F)       & \text{Negation, Verneinung, nicht}\\
    &|& (\F \land \F)    & \text{Konjunktion, und}\\
    &|& (\F \lor \F)     & \text{Disjunktion, oder}\\
    &|& (\F \implies \F) & \text{Implikation, wenn-dann}\\
    &|& (\F \iff \F)     & \text{Äquivalenz, genau-dann-wenn}\\
    &|& (\F \xor \F)     & \text{Exklusives Oder, genau-ein}\\
  \end{array}\end{displaymath}

  Formeln stehen für Parse-Bäume, Klammern werden weggelassen
\end{frame}

\begin{frame}[fragile]{Datenstruktur für aussagenlogische Formeln in ACL2}
  \dbend\hspace*{2mm}
  \parbox[t]{0.9\textwidth}{\raggedright Auf den folgenden Folien
    definieren wir eine Repräsentation aussagenlogischer Formeln als
    ACL2 Datenstruktur.  Wir verwenden dazu die gleichen
    Operatorennamen, die ACL2 für die eingebauten logischen
    Operationen verwendet.  Unterscheiden Sie genau, die Vorkommen
    einer Liste wie \texttt{(and a b)} als Teil eines ACL2-Programms
    und als aussagenlogische Formel!}

  \begin{Verbatim}[gobble=4,commandchars=\\\{\},codes={\catcode`$=3\catcode`_=8}]
    (defun f (x y)
      (if (and x y)          $\Longleftarrow$ \verbit{Teil eines ACL2 Programms}
        1
        2))

    (eval-bool '(and x y) e) $\Longleftarrow$ \verbit{Aussagenlogische Formel}
  \end{Verbatim}
  %$
\end{frame}

\begin{frame}[fragile]{Datenstruktur für aussagenlogische Formeln in ACL2}
  Als aussagenlogische Variablen $\A$ nehmen wir die Menge aller
  Symbole ohne \texttt{t} und \texttt{nil}:

  \begin{Verbatim}[commandchars=\\\{\},codes={\catcode`$=3\catcode`_=8}]
    (varp e) $\equiv$ (and (symbolp e) 
                     $\!$(not (member e '(t nil))))
  \end{Verbatim}
  %$

  Die Symbole \texttt{t} und \texttt{nil} repräsentieren $\top$ und
  $\bot$.
\end{frame}

\begin{frame}[fragile]{Datenstruktur für aussagenlogische Formeln in ACL2}
  Wir definieren eine Darstellung aussagenlogischer Formeln $\F$ als
  ACL2 Datenstruktur.

  \begin{displaymath}
    \begin{array}{rclll}
      \F & = & \A             & sym \text{ mit \texttt{(varp
                                $sym$)}}& \text{Variable}\\
         &|& \top             & \texttt{t}                    & \text{wahr}\\
         &|& \bot             & \texttt{nil}                  & \text{falsch}\\
         &|& (\lnot \F)       & \texttt{(not $e$)}            & \text{nicht}\\
         &|& (\F \land \F)    & \texttt{(and $e_1$ $e_2$)}     & \text{und}\\
         &|& (\F \lor \F)     & \texttt{(or $e_1$ $e_2$)}      & \text{oder}\\
         &|& (\F \implies \F) & \texttt{(implies $e_1$ $e_2$)} & \text{wenn-dann}\\
         &|& (\F \iff \F)     & \texttt{(iff $e_1$ $e_2$)}     & \text{genau-dann-wenn}\\
         &|& (\F \xor \F)     & \texttt{(xor $e_1$ $e_2$)}     & \text{genau-ein}\\
    \end{array}
  \end{displaymath}
\end{frame}

\begin{frame}{Beispiel}

  \begin{itemize}
  \item $A:$ Kein Ablauf des Systems führt zu einem Deadlock
  \item $B:$ Das System reagiert immer auf Eingaben
  \item $(A \implies B):$ Wenn kein Ablauf des Systems zu einem
    Deadlock führt, dann reagiert das System immer auf Eingaben
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Beispiel}

  Als logische Formel:
  \begin{displaymath}
    (((\top \lor A) \land (\bot \implies A)) \iff (\lnot\bot))
  \end{displaymath}

  Als ACL2 Datenstruktur:
\begin{Verbatim}
    (iff (and (or t a) (implies nil a))
         (not nil))
\end{Verbatim}
  
\end{frame}

\begin{frame}{Präzedenz}

  $\lnot$

  $\land$

  $\lor$

  $\xor$

  $\implies$

  $\iff$

\end{frame}

\begin{frame}[fragile]{Beispiel}

  Die Formel

  \qquad $A \land \lnot B \lor \lnot C \land D$

  steht für

  \qquad $(A \land (\lnot B)) \lor ((\lnot C) \land D$)

  das schreibt man oft als

  \qquad $(A \land \lnot B) \lor (\lnot C \land D)$

  \bigskip
  Als ACL2 Datenstruktur:
  \begin{Verbatim}[gobble=1]
    (or (and a (not b)) (and (not c) d))
  \end{Verbatim}
\end{frame}

\begin{frame}[fragile]{Beispiel}

  Die Formel

  \qquad $A \land B \implies C \iff \lnot C \implies \lnot A \lor \lnot B$

  steht für

  \qquad $((A \land B) \implies C) \iff ((\lnot C) \implies ((\lnot A) \lor
  (\lnot B)))$

  das schreibt man oft als

  \qquad $(A \land B \implies C) \iff (\lnot C \implies \lnot A \lor \lnot
  B)$


  \bigskip
  Als ACL2 Datenstruktur:
  \begin{Verbatim}[gobble=1]
    (iff (implies (and a b) c)
         (implies (not c)
                  (or (not a) (not b))))
  \end{Verbatim}
\end{frame}

\begin{frame}{Syntaktische Assoziativität}

  Rechts

\end{frame}

\begin{frame}[fragile]{Beispiel}

  Die Formel

  \qquad $A \implies B \implies C$

  steht für

  \qquad $A \implies (B \implies C$)


  \bigskip
  Unsere ACL2-Syntax legt Assoziativität eindeutig fest:
  \begin{Acl2}[gobble=1]
    (implies a (implies b c)) $\equiv A \implies (B \implies C)$
    (implies (implies a b) c) $\equiv (A \implies B) \implies C$
  \end{Acl2}
\end{frame}

\begin{frame}{Teilformeln}
  Teilformeln einer Formel können induktiv definiert werden:
  \begin{itemize}
  \item Variaben, $\top$ und $\bot$ haben nur sich selber als
    Teilformeln
  \item $(\lnot \phi)$ hat
    \begin{itemize}
    \item sich selber und 
    \item alle Teilformeln von $\phi$
    \end{itemize}
    als Teilformeln
  \item $(\phi \land \psi)$, $(\phi \lor \psi)$,
    $(\phi \implies \psi)$, $(\phi \iff \psi)$, $(\phi \xor \psi)$
    haben
    \begin{itemize}
    \item sich selber, 
    \item alle Teilformeln von $\phi$ und 
    \item alle Teilformeln von $\psi$
    \end{itemize}
    als Teilformeln.
  \end{itemize}
\end{frame}

\begin{frame}{Substitution}
  Das Ersetzen einer Teilformel durch eine beliebige andere Formel
  bezeichnet man als \emph{Substitution}:
  \begin{displaymath}
    \xi\subst\psi\phi
  \end{displaymath}
  "`In $\xi$ wird $\phi$ durch $\psi$ ersetzt."'

  Substitution lässt sich durch Rekursion über die Struktur von $\xi$
  definieren (siehe \texttt{prop-logic.lisp}).

  Manche Autoren schreiben $\xi[\psi \leftarrow \phi]$ oder
  $\xi[\psi/\phi]$ oder $\xi[\phi/\psi]$ für die angegebene
  Substitution.
\end{frame}

\section{Semantik}\label{semantik}

\begin{frame}{Semantik}

  Durch eine \emph{Semantik} wird einem syntaktischen Objekt eine
  Bedeutung gegeben.  Zum Beispiel wird in der denotationellen
  Semantik jedem Programm eine mathematische Funktion zugeordnet.

  Die Semantik der Aussagenlogik ist im Verhältnis dazu relativ
  einfach: Jeder aussagenlogischen Formel soll dadurch ein
  Wahrheitswert zugeordnet werden.
\end{frame}

\begin{frame}{Semantik}

  \begin{itemize}
  \item $\top$ ist wahr
  \item $\bot$ ist falsch
  \item $\lnot \phi$ ist wahr $\equiv$ $\phi$ ist falsch
  \item $\phi \land \psi$ ist wahr $\equiv$ $\phi$ ist wahr und $\psi$
    ist wahr
  \item $\phi \lor \psi$ ist wahr $\equiv$ $\phi$ ist wahr oder $\psi$
    ist wahr
  \item $\phi \implies \psi$ ist wahr $\equiv$ $\phi$ ist falsch oder
    $\psi$ ist wahr
  \item $\phi \iff \psi$ ist wahr $\equiv$ $\phi$ und $\psi$ sind
    beide wahr oder\\
    \hspace*{36mm}beide falsch
  \item $\phi \xor \psi$ ist wahr $\equiv$ entweder $\phi$ ist wahr
    und $\psi$ falsch\\
    \hspace*{29.3mm}oder $\phi$ ist falsch und $\psi$ wahr
  \end{itemize}
\end{frame}

\begin{frame}{Semantik von $\lor$}

  $\lor$ ist ``inklusives Oder''

  \begin{itemize}
  \item $\phi \lor \psi$ ist wahr, wenn $\phi$ wahr ist
  \item $\phi \lor \psi$ ist wahr, wenn $\psi$ wahr ist
  \item $\phi \lor \psi$ \textbf{ist wahr, wenn} $\phi$ \textbf{und}
    $\psi$ \textbf{wahr sind}
  \item $\phi \lor \psi$ ist nur dann falsch, wenn $\phi$ und $\psi$
    beide falsch sind
  \end{itemize}
\end{frame}

\begin{frame}{Semantik von $\implies$}

  $\implies$ ist ``materielle Implikation''

  \begin{itemize}
  \item $\phi \implies \psi$ \textbf{ist immer wahr, wenn} $\phi$
    \textbf{falsch ist}
  \item $\phi \implies \psi$ ist wahr, wenn

    \begin{itemize}
    \item $\phi$ wahr ist und
    \item $\psi$ auch wahr ist
    \end{itemize}
  \item $\phi \implies \psi$ \textbf{ist nur dann falsch, wenn}

    \begin{itemize}
    \item $\phi$ \textbf{wahr und}
    \item $\psi$ \textbf{falsch ist}
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Wahrheitswerte}

  Um die Definition der Semantik etwas mathematischer aufschreiben zu
  können, definieren wir erst die Menge der Wahrheitswerte $\Bool$ mit
  den üblichen Operationen.

  \begin{displaymath}
    \Bool = \{\true, \false\}
  \end{displaymath}

  Die Operationen auf $\Bool$ sind Negation, Konjunktion
  und Disjunktion.  Um sie von den Aussagenlogischen
  Junktoren zu unterscheiden, notieren wir sie als $\bnot$, $\band$
  und $\bor$:

  \begin{displaymath}
    \begin{array}{c|c}
      & \bnot\\
      \hline
      \false & \true \\
      \true  & \false
    \end{array}
    \qquad
    \begin{array}{c|c|c}
      \band  & \false & \true\\
      \hline
      \false & \false & \false\\
      \true  & \false & \true
    \end{array}
    \qquad
    \begin{array}{c|c|c}
      \bor  & \false & \true\\
      \hline
      \false & \false & \true\\
      \true  & \true & \true
    \end{array}
  \end{displaymath}

\end{frame}


\begin{frame}{Variablen?}

  Was ist der Wahrheitswert von $A \lor B$?

  Um Wahrheitswerte von \emph{Formeln} mit aussagenlogischen Variablen
  angeben zu können, müssen wir den \emph{Variablen} Wahrheitswerte
  zuweisen.  Das erfolgt mit Hilfe von sogenannten \emph{Belegungen}
  $\eta$, die Variablen in Werte aus $\Bool$ abbilden:

  \begin{displaymath}
    \eta: \A \to \Bool
  \end{displaymath}

  Damit können wir überprüfen, wie die Wahrheitswerte der Variablen
  den Wahrheitswert der Formel beeinflussen.

\end{frame}

\begin{frame}{Beispiel}

  Wir definieren die zwei einfachsten Belegungen $\eta_T$ und
  $\eta_F$, die jeder Variablen den Wert $\true$ bzw.\ $\false$
  zuweisen.

  \begin{displaymath}
  \begin{array}{c}
    \eta_T: \A \to \Bool\\
    \alpha \mapsto \true\\[1ex]
    \eta_T(A) = \true\\
    \eta_T(B) = \true\\
    \dots
  \end{array}
  \qquad
  \begin{array}{c}
    \eta_F: \A \to \Bool\\
    \alpha \mapsto \false\\[1ex]
    \eta_F(A) = \false\\
    \eta_F(B) = \false\\
    \dots
  \end{array}
  \end{displaymath}

\end{frame}

\begin{frame}{Beispiel}
  Die folgende Belegung weist der Variablen $A$ den Wert
  $\true$ und allen anderen Variablen den Wert $\false$ zu.

  \begin{align*}
    \eta_A: \A &\to \Bool\\[1ex]
    \alpha &\mapsto 
    \begin{cases}
      \true & \text{falls }\alpha = A\\
      \false & \text{sonst}
    \end{cases}\\[2ex]
    \eta_A(A) &= \true\\
    \eta_A(B) &= \false\\
    \eta_A(C) &= \false\\
    \dots
    \end{align*}
\end{frame}

\begin{frame}{Semantik von Formeln}

  Wir können jetzt die Definition der Semantik von Formeln in
  mathematischer Notation aufschreiben und auf Variablen erweitern.
  Dazu definieren wir eine Funktion $\sem{\cdot}\eta: \F \to \Bool$,
  die von einer Belegung abhängt und Formeln auf Wahrheitswerte
  abbildet.

  \begin{displaymath}
    \begin{array}{rcl}
      \sem{\top}\eta &=& \true\\
      \sem{\bot}\eta &=& \false\\
      \sem{\alpha}\eta &=& \eta(\alpha) \quad\text{für $\alpha \in \A$}\\
      \sem{\lnot \phi}\eta &=& \bnot(\sem{\phi}\eta)\\
      \sem{\phi \land \psi}\eta &=& \sem{\phi}\eta \band \sem{\psi}\eta\\
      \sem{\phi \lor \psi}\eta &=& \sem{\phi}\eta \bor \sem{\psi}\eta\\
      \sem{\phi \implies \psi}\eta &=& \bnot(\sem{\phi}\eta) \bor \sem{\psi}\eta\\
      \sem{\phi \iff \psi}\eta &=& (\sem{\phi}\eta \band \sem{\psi}\eta) \bor
      (\bnot(\sem{\phi}\eta) \band \bnot(\sem{\psi}\eta))\\
      \sem{\phi \xor \psi}\eta &=& (\sem{\phi}\eta \band \bnot(\sem{\psi}\eta)) 
                  \bor (\bnot(\sem{\phi}\eta) \band \sem{\psi}\eta)\\
    \end{array}
  \end{displaymath}
\end{frame}

\begin{frame}{Semantik von Formeln in ACL2}
  Die Semantikfunktion kann in ACL2 leicht durch eine rekursive
  Funktion implementiert werden.  Eine naheliegende Möglichkeit ist:
  \begin{align*}
    \Bool &= \{\mathtt{t}, \mathtt{nil}\}\\
    \bnot &= \mathtt{not}\\
    \band &= \mathtt{and}\\
    \bor &= \mathtt{or}\\
    \sem{~} &= \texttt{eval-bool}
  \end{align*}
  (Siehe Datei \texttt{prop-logic.lisp})
\end{frame}

\begin{frame}{Beispiel}
  Wie nicht ander zu erwarten, hängt der Wahrheitswert von $A \land B$
  von der gewählten Belegung ab:
  
  \begin{displaymath}
    \begin{array}{rcl}
      \sem{A \land B}\eta_T &=& \sem{A}\eta_T \band \sem{B}\eta_T \\
      &=& \eta_T(A) \band \eta_T(B)\\
      &=& \true \band \true\\
      &=& \true\\[3ex]
      \sem{A \land B}\eta_A &=& \sem{A}\eta_A \band \sem{B}\eta_A \\
      &=& \eta_A(A) \band \eta_A(B)\\
      &=& \true \band \false\\
      &=& \false
    \end{array}
  \end{displaymath}
\end{frame}


\begin{frame}[fragile]{Beispiel}
  Wie nicht ander zu erwarten, hängt der Wahrheitswert von $A \land B$
  von der gewählten Belegung ab:
  \begin{Acl2}[gobble=2]

    (eval-bool '(and a b) 
               '((a . t) (b . t)))   $\eval$ t

    (eval-bool '(and a b) 
               '((a . t) (b . nil))) $\eval$ nil
  \end{Acl2}
\end{frame}


\begin{frame}{Beispiel}
  Manche Formeln sind für jede Belegung falsch:

  \begin{displaymath}
    \begin{array}{rcl}
      \sem{A \land \lnot A}\eta_T &=& \sem{A}\eta_T \band \sem{\lnot A}\eta_T \\
      &=& \eta_T(A) \band \bnot(\sem{A}\eta_T)\\
      &=& \true \band \bnot(\eta_T(A))\\
      &=& \true \band \bnot(\true)\\
      &=& \true \band \false\\
      &=& \false\\[3ex]
      \sem{A \land \lnot A}\eta_F &=& \sem{A}\eta_F \band \sem{\lnot A}\eta_F \\
      &=& \eta_F(A) \band \bnot(\sem{A}\eta_F)\\
      &=& \false \band \bnot(\eta_F(A))\\
      &=& \false \band \bnot(\false)\\
      &=& \false \band \true\\
      &=& \false
    \end{array}
  \end{displaymath}
\end{frame}


\begin{frame}[fragile]{Beispiel}
  Manche Formeln sind für jede Belegung falsch:
  \begin{Acl2}[gobble=2]

    (eval-bool '(and a (not a))
               '((a . t)))          $\eval$ nil
    
    (eval-bool '(and a (not a))
               '((a . nil)))        $\eval$ nil
  \end{Acl2}
  %$
\end{frame}

\begin{frame}{Beispiel}
  Andere Formeln sind für jede Belegung wahr.
  \begin{displaymath}
    \begin{array}{rcl}
      \sem{A \lor \lnot A}\eta_T &=& \sem{A}\eta_T \bor \sem{\lnot A}\eta_T \\
      &=& \eta_T(A) \bor \bnot(\sem{A}\eta_T)\\
      &=& \true \bor \bnot(\eta_T(A))\\
      &=& \true \bor \bnot(\true)\\
      &=& \true \bor \false\\
      &=& \true\\[3ex]
      \sem{A \lor \lnot A}\eta_F &=& \sem{A}\eta_F \bor \sem{\lnot A}\eta_F \\
      &=& \eta_F(A) \bor \bnot(\sem{A}\eta_F)\\
      &=& \false \bor \bnot(\eta_F(A))\\
      &=& \false \bor \bnot(\false)\\
      &=& \false \bor \true\\
      &=& \true
    \end{array}
  \end{displaymath}
\end{frame}


\begin{frame}[fragile]{Beispiel}
  Andere Formeln sind für jede Belegung wahr.  

  \begin{Acl2}[gobble=2]

    (eval-bool '(or a (not a))
               '((a . t)))          $\eval$ t
    
    (eval-bool '(or a (not a))
               '((a . nil)))        $\eval$ t
  \end{Acl2}
  
  Derartige Eigenschaften von Formeln werden wir in den nächsten
  Folien genauer betrachten.
\end{frame}


\section{Eigenschaften}\label{eigenschaften}

\begin{frame}[fragile]{Tautologien}

  Eine Formel $\phi$ ist eine Tautologie, wenn sie für alle Belegungen
  wahr ist, wenn also gilt

  \begin{displaymath}
    \forall \eta: \A \to \Bool: \sem{\phi}\eta = \true
  \end{displaymath}

  \begin{Acl2}
    (defthm $\phi$-is-tautology
      (eval-bool $\phi$ env))
  \end{Acl2}
\end{frame}

\begin{frame}{Beispiele}
  \begin{gather*}
    A \lor \lnot A\\
    A \implies \lnot\lnot A\\
    A \iff \lnot\lnot A\\
    (A \implies B) \iff (\lnot B \implies \lnot A)\\
    (A \implies B) \land (B \implies C) \implies (A \implies C)\\
  \end{gather*}
\end{frame}

\begin{frame}[fragile]{Beispiele}
  \begin{Verbatim}
(defthm a-or-not-a-is-tautology
  (eval-bool '(or a (not a)) env))

(defthm a-implies-double-negation
  (eval-bool '(implies a (not (not a))) env))

(defthm a-iff-double-negation
  (eval-bool '(iff a (not (not a))) env))
  \end{Verbatim}
\end{frame}

\begin{frame}[fragile]{Beispiele}
  \begin{Verbatim}
(defthm contrapositive
  (eval-bool '(iff (implies a b)
                   (implies (not b) (not a)))
             env))

(defthm implies-is-transitive
  (eval-bool '(implies (and (implies a b)
                            (implies b c))
                       (implies a c))
             env))
  \end{Verbatim}
\end{frame}

\begin{frame}{Wahrheitstabellen}
  \begin{displaymath}
    \begin{array}{c||c|c}
      A      & \lnot A & A \lor \lnot A\\
      \hline
      \false & \true   & \true         \\
      \true  & \false  & \true         \\
    \end{array}
  \end{displaymath}

  Der Wahrheitswert einer Formel lässt sich durch eine
  Wahrheitstabelle für jede Belegung bestimmen.

\end{frame}

\begin{frame}{Wahrheitstabellen}

  Wahrheitstabelle für $\phi = (A \imp B) \siff (\lnot B \imp \lnot
  A)$:

  \begin{displaymath}
    \begin{array}{c|c||c|c|c|c||c}
      A      & B      & \lnot A & \lnot B & A \imp B & \lnot B \imp \lnot A & \phi\\
      \hline
      \false & \false & \true   & \true   & \true    & \true                & \true\\
      \false & \true  & \true   & \false  & \true    & \true                & \true\\
      \true  & \false & \false  & \true   & \false   & \false               & \true\\
      \true  & \true  & \false  & \false  & \true    & \true                & \true
    \end{array}
  \end{displaymath}

\end{frame}

\begin{frame}{Wahrheitstabellen}

  \begin{displaymath}
    \begin{array}{c||c|c||c|c|c}
      A      & \lnot A & \lnot\lnot A & A \lor \lnot A & A \imp \lnot\lnot A & A \siff \lnot\lnot A\\
      \hline
      \false & \true   & \false       & \true          & \true               & \true\\
      \true  & \false  & \true        & \true          & \true               & \true
    \end{array}
  \end{displaymath}

  Man kann in einer Wahrheitstabelle auch mehrere Formeln analysieren

\end{frame}

\begin{frame}{Wahrheitstabellen}

  Wir definieren $\psi = (A \imp B) \land (B \imp C)$ und $\phi = (A
  \imp B) \land (B \imp C) \imp (A \imp C)$

  \begin{displaymath}
    \begin{array}{c|c|c||c|c|c|c||c}
      A      & B      & C      & A \imp B & B \imp C & A \imp C & \psi   & \phi\\
      \hline
      \false & \false & \false & \true    & \true    & \true    & \true  & \true\\
      \false & \false & \true  & \true    & \true    & \true    & \true  & \true\\
      \false & \true  & \false & \true    & \false   & \true    & \false & \true\\
      \false & \true  & \true  & \true    & \true    & \true    & \true  & \true\\
      \true  & \false & \false & \false   & \true    & \false   & \false & \true\\
      \true  & \false & \true  & \false   & \true    & \true    & \false & \true\\
      \true  & \true  & \false & \true    & \false   & \false   & \false & \true\\
      \true  & \true  & \true  & \true    & \true    & \true    & \true  & \true
    \end{array}
  \end{displaymath}

  Problem: Die Größe der Wahrheitstabelle wächst exponentiell in der
  Anzahl der Variablen.

\end{frame}

\begin{frame}{Tautologien}

  Eine Formel ist genau dann eine Tautologie, wenn in der letzten
  Spalte ihrer Wahrheitstabelle nur der Wert $\true$ vorkommt.

\end{frame}

\begin{frame}{Erfüllbarkeit}

  Eine Formel $\phi$ ist \emph{erfüllbar} (satisfiable), wenn sie für
  mindestens eine Belegung wahr ist, wenn also gilt

  \begin{displaymath}
    \exists \eta: \A \to \Bool: \sem{\phi}\eta = \true
  \end{displaymath}

  Eine Formel ist \emph{unerfüllbar} (unsatisfiable), wenn sie nicht
  erfüllbar ist, wenn also gilt

  \begin{displaymath}
    \lnot\exists \eta: \A \to \Bool: \sem{\phi}\eta = \true
  \end{displaymath}

  oder äquivalent 
  \begin{displaymath}
    \forall \eta: \A \to \Bool: \sem{\phi}\eta = \false
  \end{displaymath}

\end{frame}

\begin{frame}{Erfüllbarkeit}

  Eine Formel $\phi$ ist genau dann erfüllbar, wenn in der letzten
  Spalte ihrer Wahrheitstabelle mindestens einmal der Wert $\true$
  vorkommt.

  \begin{displaymath}
    \begin{array}{c|c}
      \cdots & \phi\\
      \hline
      \cdots & \vdots\\
      \cdots & \true\\
      \cdots & \vdots\\
    \end{array}
  \end{displaymath}

\end{frame}

\begin{frame}{Unerfüllbarkeit}

  Eine Formel $\phi$ ist genau dann unerfüllbar, wenn in der letzten
  Spalte ihrer Wahrheitstabelle nur der Wert $\false$ vorkommt.

  \begin{displaymath}
    \begin{array}{c|c}
      \cdots & \phi\\
      \hline
      \cdots & \false\\
      \cdots & \vdots\\
      \cdots & \false\\
    \end{array}
  \end{displaymath}

\end{frame}

\begin{frame}{Äquivalenz}

  Zwei Formeln $\phi$ und $\psi$ sind äquivalent, wenn sie für alle
  Belegungen den gleichen Wahrheitswert haben, wenn also gilt

  \begin{displaymath}
    \forall \eta: \sem{\phi}\eta = \sem{\psi}\eta
  \end{displaymath}

  Wir schreiben $\phi \simeq \psi$ wenn $\phi$ und $\psi$ äquivalent
  sind.
\end{frame}

\begin{frame}{Beispiele}

  \begin{itemize}
  \item $A \implies B$ ist äquivalent zu $\lnot B \implies \lnot A$
  \item $\phi \land \psi$ ist äquivalent zu $\psi \land \phi$
  \item $(A \imp B) \siff (\lnot B \imp \lnot A)$ ist äquivalent zu
    $\top$
  \end{itemize}
\end{frame}

\begin{frame}{Erfüllbarkeitsäquivalenz}

  Zwei Formeln $\phi$ und $\psi$ sind erfüllbarkeitsäquivalent, wenn
  sie entweder beide erfüllbar oder beide unerfüllbar sind.

  \begin{displaymath}
    \exists \eta: \sem{\phi}\eta = \true \iff \exists\eta'
    \sem{\psi}\eta' = \true
  \end{displaymath}

\end{frame}

\begin{frame}{Zusammenhänge}

  $\phi$ ist eine Tautologie, genau dann, wenn $\lnot \phi$
  unerfüllbar ist:

  \begin{displaymath}
    \forall \eta: \sem{\phi}\eta = \true \equiv \forall \eta:
    \sem{\lnot\phi}\eta = \false
  \end{displaymath}

\end{frame}

\begin{frame}{Zusammenhänge}

  $\phi$ ist eine Tautologie, genau dann, wenn $\phi$ äquivalent zu
  $\top$ ist

  \begin{displaymath}
    \forall \eta: \sem{\phi}\eta = \true \equiv \forall \eta:
    \sem{\phi}\eta = \sem{\top}\eta
  \end{displaymath}

\end{frame}

\begin{frame}{Zusammenhänge}
  $\phi$ ist erfüllbar, genau dann, wenn $\phi$
  erfüllbarkeitsäquivalent zu $\top$ ist

  \begin{displaymath}
    \exists \eta: \sem{\phi}\eta = \true 
    \equiv 
    \bigl(
      \exists \eta: \sem{\phi}\eta = \true 
      \equiv 
      \exists \eta': \sem{\top}\eta' = \true
    \bigr)
  \end{displaymath}
\end{frame}

\begin{frame}{Zusammenhänge}
  $\phi$ ist genau dann äquivalent zu $\psi$, wenn $\phi \siff \psi$
  eine Tautologie ist

  \begin{displaymath}
    \forall \eta: \sem{\phi}\eta = \sem{\psi}\eta 
    \equiv 
    \forall \eta: \sem{\phi \siff \psi}\eta = \true
  \end{displaymath}
\end{frame}



\begin{frame}{Automatisches Beweisen (I)}
  \begin{itemize}
  \item Durch systematisches Ausprobieren aller Belegungen kann man
    feststellen, ob die Formel
    \begin{itemize}
    \item allgemeingültig,
    \item erfüllbar oder
    \item unerfüllbar
    \end{itemize}
    ist.  Für erfüllbare Formeln kann man so auch eine erfüllende
    Belegung finden.
  \item Ein solches Verfahren nennt man eine
    \emph{Entscheidungsprozedur} oder ein
    \emph{Entscheidungsverfahren}.
  \item Die Verifikation eines Systems durch Ausprobieren aller
    möglichen Systemzustände nennt sich \emph{Model Checking}.
  \item (Manche Autoren verwenden den Begriff aber ausschließlich für
    die Verifikation temporallogischer Spezifikationen.)
  \end{itemize}
\end{frame}

\section{Beweissystem}\label{beweissystem}

\begin{frame}{Syntaktische Beweise}
  \begin{itemize}
  \item Bisherige Beweise "`semantisch"', durch Ausprobieren aller
    Belegungen.
  \item Eine aussagenlogische Formel mit $n$ Variablen hat $2^n$
    Belegungen.
  \item Bei Systemen mit großen oder unendlichen Modellen geht das
    nicht mehr.
  \item Computer sind gut im Manipulieren von Syntax.
  \item Wir hätten gerne ein syntaktisches Beweisverfahren.
  \item (In der Aussagenlogik ist der Unterschied zwischen beiden
    Verfahern nicht sehr groß, da alle Modelle von Aussagenlogischen
    Formeln endlich sind.)
  \end{itemize}
\end{frame}

\begin{frame}{Syntaktische Beweise}
  Ein syntaktisches Beweisverfahren versucht Formeln durch Umformung
  auf eine Form zu bringen, aus der sich die Allgemeingültigkeit oder
  Erfüllbarkeit leicht ablesen lässt.  Typischerweise haben
  Beweisverfahren
  \begin{itemize}
  \item Axiome(nschemata) und
  \item Schlussregeln.
  \end{itemize}
  Wir verwenden ein Beweisverfahren, das an das von ACL2 verwendete
  Verfahren angelehnt ist.
\end{frame}

\begin{frame}{Elimination von Operatoren}
  Um nicht zu viele Axiome und Inferenzregeln zu benötigen kann man
  $\land$, $\implies$, $\iff$ und $\xor$ durch $\lnot$ und $\lor$
  ersetzen:
  \begin{align*}
    \phi \land \psi &\simeq \lnot((\lnot \phi) \lor (\lnot \psi))\\
    \phi \implies \psi &\simeq \lnot \phi \lor \psi\\
    \phi \iff \psi &\simeq (\phi \implies \psi) \land (\psi \implies \phi)\\
      &\simeq \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot(\phi \lor \psi)\\
    \phi \xor \psi &\simeq (\lnot\phi \land \psi) \lor (\phi \land \lnot\psi)\\
      &\simeq \lnot(\phi \lor \lnot \psi) \lor \lnot(\lnot \phi \lor \psi)
  \end{align*}

\end{frame}

\begin{frame}{Beweiskalkül}
  Wir verwenden einen Beweiskalkül mit dem Axiomenschema
  \begin{itemize}
  \item Tertium non datur:\\
    $\lnot\phi \lor \phi$
  \end{itemize}
  und folgenden Inferenzregeln:
  \begin{itemize}
  \item Expansion:\\
    Aus $\psi$ lässt sich $\phi \lor \psi$ ableiten.
  \item Kontraktion:\\
    Aus $\phi \lor \phi$ lässt sich $\phi$ ableiten.
  \item Assoziativität:\\
    Aus $\phi \lor(\psi \lor \xi)$ lässt sich
    $(\phi \lor \psi) \lor \xi$ ableiten
  \item Schnittregel:\\
    Aus $\phi \lor \psi$ und $\lnot \phi \lor \xi$ lässt sich
    $\psi \lor \xi$ ableiten.
  \item Instanziierung:\\
    Aus $\phi$ lässt sich $\phi\subst \psi v$ für jede Variable $v$ und
    jeden Term $\psi$ ableiten.
  \end{itemize}
\end{frame}

\begin{frame}{Beispiel}
  $A \iff \lnot\lnot A$ ist eine Tautologie:
  \begin{displaymath}
    \begin{array}{lll}
      1. & \lnot A \lor A& \text{Axiom}\\
      2. & \lnot\lnot A \lor \lnot A & \text{Axiom}\\
      3. & \lnot\lnot\lnot A \lor \lnot\lnot A & \text{Axiom}\\
      4. & \lnot\lnot\lnot\lnot A \lor \lnot\lnot\lnot A & \text{Axiom}\\
      5. & A \lor \lnot A & \text{Schnittregel 1.,2.}\\
      6. & \lnot A \lor \lnot\lnot A  & \text{Schnittregel 2.,3.}\\
      7. & \lnot\lnot A \lor \lnot\lnot\lnot A  & \text{Schnittregel 3.,4.}\\
      8. & A \lor \lnot\lnot\lnot A & \text{Schnittregel 1.,7.}\\
      9. & \lnot\lnot\lnot A \lor A & \text{Schnittregel 8.,1.} \\
    \end{array}
    \end{displaymath}
    6. ist $A \implies \lnot\lnot A$, 9. ist
    $\lnot\lnot A \implies A$.  Wir müssen noch zeigen, dass auch die
    Konjunktion dieser Formeln gilt.
\end{frame}

\begin{frame}{Beispiel: Ableitung der Konjunktion $\phi\land\psi$}
  \vspace*{-3mm}
  \begin{displaymath}
    \begin{array}{rll}
      1./2. & \phi, \psi & \text{Gegeben}\\
      3. & \lnot(\lnot \phi \lor \lnot \psi) \lor (\lnot \phi \lor \lnot \psi) & \text{Axiom}\\
      4. & \underbrace{(\lnot(\lnot \phi \lor \lnot \psi) \lor \lnot \phi)}_\alpha
           \lor \lnot \psi & \text{Assoziativität}\\
      5. & \lnot \alpha \lor \alpha & \text{Axiom}\\
      6. & \lnot \psi \lor \alpha & \text{Schnitt 4.,5.}\\
      7. & \alpha \lor \psi& \text{Expansion 2.}\\
      8. & \psi \lor \alpha & \text{Schnitt 7.,5.} \\
      9. & \alpha \lor \alpha & \text{Schnitt 6.,8.} \\
      10. & \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot \phi & \text{Kontraktion} \\
      11. & \lnot\lnot(\lnot \phi \lor \lnot \psi) \lor
            \lnot(\lnot \phi \lor \lnot \psi) & \text{Axiom} \\
      12. & \lnot\phi \lor \lnot(\lnot \phi \lor \lnot \psi) & \text{Schnitt 10.,11.} \\
      13. & \lnot(\lnot \phi \lor \lnot \psi) \lor \phi & \text{Expansion 1.} \\
      14. & \phi \lor \lnot(\lnot \phi \lor \lnot \psi) & \text{Schnitt 13.,11.} \\
      15. & \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot(\lnot \phi \lor \lnot \psi)
                & \text{Schnitt 14.,12.} \\
      16. & \lnot(\lnot \phi \lor \lnot \psi) & \text{Kontraktion} \\
    \end{array}
    \end{displaymath}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
