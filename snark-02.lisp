(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf)
  (asdf:load-system :snark)
  (require :snark))

(in-package "SNARK-USER")

(defun set-up-snark ()
  (initialize)
  (print-options-when-starting nil)
  (print-rows-when-derived nil)
  (print-summary-when-finished nil)
  (print-agenda-when-finished nil)
  (print-final-rows t)
  (use-resolution t)
  (use-paramodulation t))

(defun set-up-simple-resolution ()
  (set-up-snark)
  (assert '(implies (p ?x) (q ?x)))
  (assert '(implies (p ?x) (p (f ?x))))
  (assert '(implies (p (f ?x)) (p (g ?x))))
  (assert '(p c)))

(defun prove-simple-1 ()
  (set-up-simple-resolution)
  (new-prove '(p (f c))))

(defun prove-simple-2 ()
  (set-up-simple-resolution)
  (new-prove '(q (f c))))

(defun prove-simple-3 ()
  (set-up-simple-resolution)
  (new-prove '(q (g (f (g c))))))

(defun print-n-answers (&optional (n 5))
  (let ((result (loop for i from 1 to n
		   when (answer) collect (second (answer))
		   do (closure))))
    (loop for i from 1
       and answer in result
       do
	 (format t "~&The ~:R answer is~,3T~A." i answer))))

(defun prove-simple-4 ()
  (set-up-simple-resolution)
  (prog1
      (new-prove '(q ?x) :answer '(ans ?x))
    (format t
"~&Type (closure) at the command prompt several times to generate
individual answers.  Or type (print-n-answers) at the command prompt
to print multiple answers.")))
