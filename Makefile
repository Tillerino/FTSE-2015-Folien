slides = lecture-01.pdf lecture-02.pdf lecture-03.pdf \
         lecture-04.pdf lecture-05.pdf lecture-06.pdf \
         lecture-07.pdf lecture-08.pdf

%.pdf : %.tex
	pdflatex $<

all: $(slides)
