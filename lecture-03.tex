\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{turnstile}
\usepackage{fancyvrb}
\usepackage{manfnt}

\input{Macros/macros.tex}

\title{Formale Techniken der Software-Entwicklung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}


\section{Beweissystem}\label{beweissystem}

\begin{frame}{Syntaktische Beweise}
  Ein syntaktisches Beweisverfahren versucht die Allgemeingültigkeit
  oder Erfüllbarkeit von Formeln durch rein syntaktische Mittel zu
  zeigen.  Typischerweise haben Beweisverfahren
  \begin{itemize}
  \item Axiome/Axiomennschemata und
  \item Schlussregeln.
  \end{itemize}
  Wir verwenden ein Beweisverfahren, das an das von ACL2 verwendete
  Verfahren angelehnt ist und zur Klasse der
  sog. \emph{Hilbertkalküle} zählt.
\end{frame}

\begin{frame}{Syntaktische Beweise}  
  Ein Beweis ist eine Folge von aussagenlogischen Formeln, die nach
  folgenden Regeln aufgebaut wird:
  \begin{itemize}
  \item \emph{Axiome} und \emph{Instanziierungen von Axiomenschemata}
    können an jeder Position der Folge vorkommen
  \item Formeln können eingefügt werden, wenn sie durch
    \emph{Schlussregeln} aus vorhergehenden Formeln in der Folge
    abgeleitet werden können
  \end{itemize}
  Jede Formel, die im Beweis vorkommt ist (syntaktisch) bewiesen.
\end{frame}

\begin{frame}{Elimination von Operatoren}
  Um nicht zu viele Axiome und Inferenzregeln zu benötigen kann man
  $\land$, $\implies$, $\iff$ und $\xor$ durch $\lnot$ und $\lor$
  ersetzen:
  \begin{align*}
    \phi \land \psi &\simeq \lnot((\lnot \phi) \lor (\lnot \psi))\\
    \phi \implies \psi &\simeq \lnot \phi \lor \psi\\
    \phi \iff \psi &\simeq (\phi \implies \psi) \land (\psi \implies \phi)\\
      &\simeq \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot(\phi \lor \psi)\\
    \phi \xor \psi &\simeq (\lnot\phi \land \psi) \lor (\phi \land \lnot\psi)\\
      &\simeq \lnot(\phi \lor \lnot \psi) \lor \lnot(\lnot \phi \lor \psi)
  \end{align*}
\end{frame}

\begin{frame}{Beweiskalkül}
  Wir verwenden einen Beweiskalkül (ACL2) mit dem Axiomenschema
  \begin{itemize}
  \item Tertium non datur:\\
    $\lnot\phi \lor \phi$
  \end{itemize}
  und folgenden Inferenzregeln:
  \begin{itemize}
  \item Expansion:\\
    Aus $\psi$ lässt sich $\phi \lor \psi$ ableiten.
  \item Kontraktion:\\
    Aus $\phi \lor \phi$ lässt sich $\phi$ ableiten.
  \item Assoziativität:\\
    Aus $\phi \lor(\psi \lor \xi)$ lässt sich
    $(\phi \lor \psi) \lor \xi$ ableiten
  \item Schnittregel:\\
    Aus $\phi \lor \psi$ und $\lnot \phi \lor \xi$ lässt sich
    $\psi \lor \xi$ ableiten.
  \item Instanziierung:\\
    Aus $\phi$ lässt sich $\phi\subst \psi v$ für jede Variable $v$ und
    jeden Term $\psi$ ableiten.
  \end{itemize}
\end{frame}

\begin{frame}{Beispiel}
  $A \iff \lnot\lnot A$ ist eine Tautologie:
  \begin{displaymath}
    \begin{array}{lll}
      0. & \lnot A \lor A& \text{Axiom}\\
      1. & \lnot\lnot A \lor \lnot A & \text{Axiom}\\
      2. & \lnot\lnot\lnot A \lor \lnot\lnot A & \text{Axiom}\\
      3. & \lnot\lnot\lnot\lnot A \lor \lnot\lnot\lnot A & \text{Axiom}\\
      4. & A \lor \lnot A & \text{Schnittregel 0.,1.}\\
      5. & \lnot A \lor \lnot\lnot A  & \text{Schnittregel 1.,2.}\\
      6. & \lnot\lnot A \lor \lnot\lnot\lnot A  & \text{Schnittregel 2.,3.}\\
      7. & A \lor \lnot\lnot\lnot A & \text{Schnittregel 0.,6.}\\
      8. & \lnot\lnot\lnot A \lor A & \text{Schnittregel 7.,0.} \\
    \end{array}
    \end{displaymath}
    5. ist $A \implies \lnot\lnot A$, 8. ist
    $\lnot\lnot A \implies A$.  Wir müssen noch zeigen, dass auch die
    Konjunktion dieser Formeln gilt.  

    Wir bezeichnen dazu $A \implies \lnot\lnot A$ als $\phi$ und
    $\lnot\lnot A \implies A$ als $\psi$.
\end{frame}

\begin{frame}{Beispiel: Ableitung der Konjunktion aus 5. und 8.}
  \vspace*{-3mm}
  \begin{displaymath}
    \begin{array}{rll}
      5./8. & \phi, \psi & \text{Gegeben}\\
      9. & \lnot(\lnot \phi \lor \lnot \psi) \lor (\lnot \phi \lor \lnot \psi) & \text{Axiom}\\
      10. & \underbrace{(\lnot(\lnot \phi \lor \lnot \psi) \lor \lnot \phi)}_\alpha
           \lor \lnot \psi & \text{Assoziativität 9.}\\
      11. & \lnot \alpha \lor \alpha & \text{Axiom}\\
      12. & \lnot \psi \lor \alpha & \text{Schnitt 10.,11.}\\
      13. & \alpha \lor \psi& \text{Expansion 8.}\\
      14. & \psi \lor \alpha & \text{Schnitt 13.,11.} \\
      15. & \alpha \lor \alpha & \text{Schnitt 14.,12.} \\
      16. & \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot \phi &
                                                                \text{Kontraktion 15.} \\
      17. & \lnot\lnot(\lnot \phi \lor \lnot \psi) \lor
            \lnot(\lnot \phi \lor \lnot \psi) & \text{Axiom} \\
      18. & \lnot\phi \lor \lnot(\lnot \phi \lor \lnot \psi) & \text{Schnitt 16.,17.} \\
      19. & \lnot(\lnot \phi \lor \lnot \psi) \lor \phi & \text{Expansion 5.} \\
      20. & \phi \lor \lnot(\lnot \phi \lor \lnot \psi) & \text{Schnitt 19.,17.} \\
      21. & \lnot(\lnot \phi \lor \lnot \psi) \lor \lnot(\lnot \phi \lor \lnot \psi)
                & \text{Schnitt 20.,18.} \\
      22. & \lnot(\lnot \phi \lor \lnot \psi) & \text{Kontraktion 21.} \\
    \end{array}
    \end{displaymath}
\end{frame}

\begin{frame}{Proof Checker und interaktive Theorembeweiser}
  \begin{itemize}
  \item Ein Beweis ist korrekt wenn er gewissen syntaktischen Regeln
    gehorcht
  \item Das lässt sich leicht durch ein Programm verifizieren
  \item Ein solches Programm nennt sich "`Proof Checker"'
  \item Ein interaktiver Theorembeweiser hilft beim Erstellen von
    Beweisen indem er zeigt, wie sich die Anwendung von
    Beweisschritten auf den Aktuellen Zustand eines Beweises auswirkt
  \item Die meisten interaktiven Theorembeweiser bieten
    Beweisstrategien, die viele Anwendungen von Ableitungsregeln
    zusammenfassen und erlauben Benutzern die Definition von
    eigenen Beweisstrategien
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Einschub: Quasiquote in ACL2}
  \begin{itemize}
  \item In ACL2 will man oft Listen erzeugen, die \emph{fast} Literale
    sind, in denen aber einzelne Elemente ausgewertet werden sollen:
    \begin{Verbatim}[gobble=4]
      (defun negate (term)
        (list 'not term))
      (defun contrapositive (phi psi)
        (list 'iff
              (list 'implies phi psi)
              (list 'implies (list 'not psi)
                             (list 'not phi))))
    \end{Verbatim}
  \item Die Struktur der Listen ist in diesem Fall nicht sehr gut zu
    erkennen
  \item Schöner wäre
    \begin{Acl2}[gobble=4]
      (defun negate (\verbit{term})
        '(not \verbit{term}))
    \end{Acl2}
    aber dabei wird \verbit{term} in der Liste nicht ausgewertet
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Einschub: Quasiquote in ACL2}
  \begin{itemize}
  \item Es gibt eine alternative Form Listen zu schreiben, die es
    erlaubt, einzelne Elemente der Liste auszuwerten
  \item Dazu verwendet man statt dem normalen Hochkomma\\ (\texttt{'},
    Quote) das "`Gegenhochkomma"' (\texttt{`}, Backquote)
  \item Die Backquote verhindert (genau wie die Quote) die Auswertung
    des nachfolgenden Ausdrucks
  \item Durch ein vorangestelltes Komma (\texttt{,}) wird ein Ausdruck
    innerhalb der Backquote ausgewertet 
    \begin{Verbatim}[gobble=4]
      (defun negate (term)
        `(not ,term))
      (defun contrapositive-2 (phi psi)
        `(iff (implies ,phi ,psi)
              (implies (not ,psi) (not ,phi))))
    \end{Verbatim}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Einschub: Quasiquote in ACL2}
  \begin{itemize}
  \item Es ist auch möglich Listen wie mit \texttt{append}
    einzufügen.\\
    Die Funktion
    \begin{Verbatim}[gobble=4]
      (defun insert-arguments (args)
        (cons 'op (append args (list 'x))))
    \end{Verbatim}
    fügt eine Liste von Argumenten zwischen \texttt{op} und \texttt{x}
    ein.
    \begin{Acl2}[gobble=4]
      (insert-arguments '(1 2))  \eval (OP 1 2 X)
    \end{Acl2}
  \item Mit Quasiquote:
    \begin{Acl2}[gobble=4]
      (defun insert-arguments (args)
        `(op ,@args x))
      (insert-arguments '(1 2))  \eval (OP 1 2 X)
    \end{Acl2}
  \end{itemize}
\end{frame}

\begin{frame}{Abgeleitete Schlussregeln}
  Der Beweis von
  $(A \implies \lnot\lnot A) \land (\lnot\lnot A \implies A)$ nimmt
  keinen Bezug auf die Struktur
  \begin{itemize}
  \item der beiden Aussagen
  \item der Ableitung der Aussagen
  \end{itemize}
  Wir können also jeden Beweis von zwei beliebigen Aussagen $\phi$ und
  $\psi$ auf diese Weise zu einem Beweis von $\phi \land \psi$
  fortsetzen.

  Schlussregeln, die Beweise abkürzen nennt man \emph{abgeleitete
    Schlussregeln}.
\end{frame}

\begin{frame}{Beispiele für abgeleitete Regeln}
  \begin{itemize}
  \item Leite aus $\phi \lor \psi$ die Formel $\psi \lor \phi$ her
  \item Leite aus $\phi$ und $\psi$ die Formel $\phi \land \psi$ her\\
    (Natürlich schreibt man im Beweiskalkül dafür
    $\lnot(\lnot \phi \lor \lnot\psi)$)
  \item Leite aus $\phi \land \psi$ die Formel $\phi$ her\\
    (Ebenso für $\psi$)
  \item Leite aus $\phi$ und $\phi \implies \psi$ die Formel $\psi$
    her\\ (Modus Ponens)
  \item Leite aus $\phi \implies \psi$ die Formel $\lnot\psi \implies
    \lnot\phi$ her\\ (indirekter Beweis, Kontrapositiv)
  \end{itemize}
\end{frame}

\begin{frame}{Beweiskalkül}
  \begin{itemize}
  \item Kann man mit den Schlussregeln falsche Aussagen ableiten?\\
    (Korrektheit)
  \item Gibt es wahre Aussagen, die man nicht ableiten kann?\\
    (Adäquanz)
  \item Warum braucht man überhaupt Schlussregeln?\\
    (Automatisierung)
  \end{itemize}
\end{frame}

% \begin{frame}{Korrektheit}
%   \begin{itemize}
%   \item Zu zeigen:
%     \begin{itemize}
%     \item Wenn alle Voraussetzungen einer Schlussregel wahr sind
%     \item Dann ist auch die Konklusion wahr
%     \end{itemize}
%   \item Induktion über die Struktur der Herleitung
%   \end{itemize}
% \end{frame}

% \begin{frame}{Vollständigkeit}
%   \begin{itemize}
%   \item Zu zeigen:
%     \begin{itemize}
%     \item Wenn eine Aussage eine Tautologie ist
%     \item Dann kann man das auch mit dem Beweiskalkül beweisen
%     \end{itemize}
%   \item Beweis: Finden eines Ableitungsbaumes für jede wahre Formel
%   \end{itemize}
% \end{frame}

\begin{frame}
  \frametitle{Wiederholung: Beweis durch Wahrheitstabelle}
  Sei $\chi = \lnot(\phi \land \psi) \iff \lnot \phi \lor \lnot \psi$
  \begin{displaymath}
    \begin{array}{c|c||c|c|c|c|c||c}
      \phi      & \psi      & \lnot \phi & \lnot \psi & \phi \land \psi & \lnot(\phi \land \psi) & \lnot \phi \lor \lnot \psi & \chi \\
      \hline\hline
      \false & \false & \true   & \true   & \false    & \true            & \true                & \true\\
      \false & \true  & \true   & \false  & \false    & \true            & \true                & \true\\
      \true  & \false & \false  & \true   & \false    & \true            & \true                & \true\\
      \true  & \true  & \false  & \false  & \true     & \false           & \false               & \true\\
    \end{array}
  \end{displaymath}
  Entsprechend zeigt man die Allgemeingültigkeit von $\lnot(\phi \lor
  \psi) \iff \lnot \phi \land \lnot \psi$.
\end{frame}

\begin{frame}
  \frametitle{De Morgan'sche Regeln}
  \begin{align*}
    \lnot(\phi \land \psi) &\iff \lnot \phi \lor \lnot \psi\\
    \lnot(\phi \lor \psi) &\iff \lnot \phi \land \lnot \psi\\
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Konjunktion/Disjunktion von Formelmengen}
  Wir definieren
  \begin{displaymath}
    \Land \{\phi_1, \dots, \phi_n\} = \Land_{i=1}^n \phi_i = \phi_1 \land \dots \land \phi_n 
  \end{displaymath}
  und
  \begin{displaymath}
    \Lor \{\phi_1, \dots, \phi_n\} = \Lor_{i=1}^n \phi_i = \phi_1 \lor \dots \lor \phi_n
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Konjunktion/Disjunktion von Formelmengen}
  Oft lassen wir dabei die Mengenklammern weg und schreiben
  \begin{displaymath}
    \Land \phi_1, \dots, \phi_n
  \end{displaymath}
  oder
  \begin{displaymath}
    \Land \Gamma
  \end{displaymath}
  Wir definieren die Spezialfälle
  \begin{displaymath}
    \Land \emptyset = \true \qquad \Lor \emptyset = \false
  \end{displaymath}

  Oft schreiben wir auch einfach $\Gamma$ für $\Land \Gamma$.

  Dann schreiben wir auch $\Gamma, \phi$ für
  $\Land \Gamma \land \phi$.
\end{frame}

\begin{frame}
  \frametitle{Notation}
  Formel $\phi$ ist wahr unter Belegung $\eta$
  \begin{displaymath}
    \models_\eta \phi \text{\quad oder\quad} \eta \models \phi
  \end{displaymath}
  Formel $\phi$ ist eine Tautologie (d.h. wahr unter jeder Belegung)
  \begin{displaymath}
    \models \phi
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Semantischer Folgerungsbegriff}
  $\psi$ folgt semantisch aus $\Gamma$ (d.h., aus $\Land \Gamma$)
  \begin{displaymath}
    \Gamma \models \psi
  \end{displaymath}
  genau dann, wenn jede Belegung, die $\Gamma$ erfüllt auch $\psi$
  erfüllt:
  \begin{displaymath}
    \text{für alle $\eta$ gilt\quad} \models_\eta \Gamma
    \text{\quad impliziert\quad} \models_\eta \psi
  \end{displaymath}
\end{frame}

% \begin{frame}
%   \frametitle{Semantischer Folgerungsbegriff}
%   $\psi_1, \dots, \psi_n$ folgt semantisch aus $\phi_1, \dots \phi_m$
%   \begin{displaymath}
%     \phi_1, \dots, \phi_m \models \psi_1, \dots, \psi_n
%   \end{displaymath}
%   genau dann, wenn jede Belegung, die alle $\phi_i$ erfüllt auch
%   (mindestens) ein $\psi_j$ erfüllt, wenn also gilt
%   \begin{displaymath}
%     \Land \phi_1, \dots, \phi_m \models \Lor \psi_1, \dots, \psi_n
%   \end{displaymath}
% \end{frame}

\begin{frame}
  \frametitle{Deduktionstheorem}
  Seien $\Gamma$ eine endliche Menge von Formeln, $\phi$ und $\psi$ Formeln.
  Dann gilt
  \begin{equation}
    \Gamma, \phi \models \psi
  \end{equation}
  genau dann, wenn
  \begin{equation}
    \Gamma \models \phi \implies \psi
  \end{equation}
  gilt.
\end{frame}

\begin{frame}
  \frametitle{Deduktionstheorem und Syntaktische Beweise}
  Das Deduktionstheorem ermöglicht oft eine vereinfachte Form
  Ableitungen aufzuschreiben
  \begin{itemize}
  \item Um $\Gamma \models \phi$ zu zeigen betrachten wir nur Modelle,
    in denen alle Formeln aus $\Gamma$ gelten
  \item Im Beweis können wir dann $\Gamma$ als gegeben annehmen
  \end{itemize}
  Somit erhalten wir als Beweisverfahren
  \begin{itemize}
  \item Führe die Formel $\phi$ als Axiom ein
  \item Zeige damit $\psi$
  \item Dann ist $\phi \implies \psi$ (ohne Axiome) gezeigt
  \end{itemize}
  Das ist keine abgeleitete Schlussregel, erweitert aber ebenfalls die
  Menge der beweisbaren Aussagen nicht.
\end{frame}

\begin{frame}{Deduktionstheorem}
  \begin{displaymath}
    \begin{array}{lll}
      0. & (A \implies B) \land (B \implies C)& \text{Axiom}\\
      1. & A \implies B & \land_L\\
      2. & B \implies C & \land_R\\
      3. & A & \text{Axiom}\\
      4. & B & \text{Modus Ponens 3.,1.}\\
      5. & C & \text{Modus Ponens 4.,2.}\\
      6. & A \implies C & \text{Deduktionsth. 3.,5.}\\
      7. & (A \implies B) \land (B \implies C) \implies (A \implies C) &
      \text{Deduktionsth. 0.,6.}
    \end{array}
    \end{displaymath}

\end{frame}

\begin{frame}{Deduktionstheorem}
  \dbend\hspace*{2mm}
  \parbox[t]{0.9\textwidth}{\raggedright Auf Axiome, die durch das
    Deduktionstheorem eingeführt werden (die Formel $\phi$) dürfen im
    Beweis keine Substitutionen angewendet werden!}
  \begin{displaymath}
    \begin{array}{lll}
      0. & A \implies \bot& \text{Axiom}\\
      1. & \top \implies \bot & \text{Substitution $\top$ für $A$.
                                \textbf{FALSCH}}\\
      2. & (A \implies \bot) \implies (\top \implies \bot) &
                                                             \text{Deduktionstheorem
                                                             0.,1.}\\
      3. & (\bot \implies \bot) \implies (\top \implies \bot) &
                                                                \text{Substitution
                                                                2.}\\
      4. & \bot \implies \bot & \text{Tertium non Datur}\\
      5. & \top \implies \bot & \text{Modus Ponens 4.,3.}\\
    \end{array}
    \end{displaymath}
    Damit haben wir die falsche Formel $\top \implies \bot$ abgeleitet!
\end{frame}

\begin{frame}{Deduktionstheorem}
  \frametitle{Beweis}
  $(1) \imp (2)$: Es gilt $\Gamma, \phi \models \psi$.  Sei $\eta$
  eine Belegung, die $\Gamma$ erfüllt.  Wenn $\eta$ $\phi$ nicht
  erfüllt, so gilt $\phi \imp \psi$ und somit (2).  Erfülle also
  $\eta$ $\phi$.  Damit erfüllt $\eta$ auch $\Gamma, \phi$, und da
  $\Gamma, \phi \models \psi$ gilt, erfüllt $\eta$ dann auch
  $\psi$. Somit erfüllt $\eta$ auch $\phi \implies \psi$.

  $(2) \imp (1)$: Es gilt $\Gamma \models \phi \implies \psi$. Sei
  $\eta$ eine Belegung, die $\Gamma, \phi$ (und damit auch $\Gamma$
  und $\phi$ einzeln) erfüllt.  Da $\eta \models \Gamma$ gilt, erfüllt
  $\eta$ auch $\phi \imp \psi$, somit erfüllt $\eta$ auch $\psi$, und
  damit gilt $\Gamma, \phi \models \psi$.
\end{frame}

\begin{frame}
  \frametitle{Verallgemeinerte De Morgan'sche Regeln}
  Man kann die De Morgan'schen Regeln leicht auf Formelmengen
  verallgemeinern:
  \begin{align*}
    \lnot\Bigl(\Land_{i=1}^n \phi_i\Bigr) &\iff \Lor_{i=1}^n \lnot \phi_i\\
    \lnot\Bigl(\Lor_{i=1}^n \phi_i \Bigr) &\iff \Land_{i=1}^n \lnot \phi_i\\
  \end{align*}
  (Beweis durch Induktion über $n$.)
\end{frame}

\begin{frame}
  \frametitle{Sequenzen}
  Eine Sequenz $\seq{\Gamma}{\phi}$ entspricht einer Implikation:
  $\Land \Gamma \imp \psi$

  Wir können mit der Definition von $\imp$ und den De Morgan'schen
  Regeln folgendermaßen umformen
  \begin{eqnarray*}
    \Land \phi_i &\implies& \psi\\
    \lnot(\Land \phi_i) &\lor& \psi\\
    \Lor \lnot \phi_i &\lor& \psi_j
  \end{eqnarray*}

  Es ist also $\seq{\phi_1, \dots, \phi_m}{\psi}$ äquivalent zu
  \begin{displaymath}
    \lnot \phi_1 \lor \dots \lor \lnot \phi_m \lor \psi
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Notation}
  Eine Sequenz $\seq{\Gamma}{\phi}$ ist im ACL2 Beweiskalkül
  ableitbar
  \begin{displaymath}
    \derivesacl \seq{\Gamma}{\phi} \quad\text{oder}\quad \Gamma
    \derivesacl \phi
  \end{displaymath}

  Eine Formel $\phi$ ist im ACL2 Beweiskalkül ableitbar:
  \begin{displaymath}
    \derivesacl{\phi}
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Korrektheit und Adäquanz und Vollständigkeit}
  \begin{itemize}
  \item Korrektheit
    \begin{displaymath}
      \Gamma \derivesacl \phi \qtext{impliziert} \Gamma \models \phi
    \end{displaymath}
  \item Adäquanz
    \begin{displaymath}
      \Gamma \models \phi \qtext{impliziert} \Gamma \derivesacl \phi
    \end{displaymath}
  \item Vollständigkeit
    \begin{displaymath}
      \Gamma \models \phi \qtext{genau dann, wenn} \Gamma \derivesacl \phi
    \end{displaymath}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Vollständigkeitssatz}
  Der Beweiskalkül ACL2 ist vollständig.

  \textbf{Beweisidee:} Wir müssen Korrektheit und Adäquanz zeigen.

  Korrektheit: Angenommen wir haben einen Beweis, der höchstens die
  Axiome $\Gamma$ enthält.  Dann müssen wir für jede Anwendung einer
  Inferenzregel im Beweis zeigen: Falls eine Belegung $\eta$ die
  Voraussetzungen der Regel erfüllt, dann gilt auch die abgeleitete
  Formel.

  Adäquanz zu zeigen ist schwieriger: Hier müssen wir zeigen, dass wir
  eine Ableitung von $\phi$ aus $\Gamma$ konstruieren können, wenn
  $\Gamma\models\phi$ gilt.  Dazu geht man in drei Schritten vor:
\end{frame}

\begin{frame}
  \frametitle{Vollständigkeitssatz: Adäquanz}
  Eine Formelmenge heißt \emph{syntaktisch konsistent}, wenn gilt
  $\Gamma \not\derivesacl \bot$ (aus $\Gamma$ lässt sich nicht $\bot$
  ableiten).

  Eine Formelmenge $H$ heißt \emph{Hintikka-Menge}, wenn gilt
  \begin{itemize}
  \item Konstanten: $\bot\not\in H$
  \item Aussagenvariablen: Es gibt kein $A$ mit $A \in H$ und
    $\lnot A \in H$
  \item Negation: Aus $\lnot\lnot \phi \in H$ folgt $\phi \in H$
  \item ($\alpha$)-Formeln: Aus $\lnot(\phi \lor \psi) \in H$ folgt
    $\lnot\phi \in H$ und $\lnot \psi \in H$
  \item ($\beta$)-Formeln: Aus $\phi \lor \psi\in H$ folgt $\phi \in H$ oder $\psi \in H$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Vollständigkeitssatz: Adäquanz}
  \begin{itemize}
  \item Adäquanz ist äquivalent zur Aussage, dass jede syntaktisch
    konsistente Formelmenge ein Modell hat
  \item Jede Hintikka-Menge hat ein Modell
  \item Zu jeder syntaktisch konsistenten Formelmenge $\Gamma$ lässt
    sich eine Hintikka-Menge $\Delta$ konstruieren, für die $\Gamma
    \subseteq \Delta$ gilt
  \end{itemize}
\end{frame}


\begin{frame}{Beweiskalkül}
  Wir verwenden einen Beweiskalkül (ACL2) mit dem Axiomenschema
  \begin{itemize}
  \item Tertium non datur:\\
    $\lnot\phi \lor \phi$
  \end{itemize}
  und folgenden Inferenzregeln:
  \begin{itemize}
  \item Expansion:\\
    Aus $\psi$ lässt sich $\phi \lor \psi$ ableiten.
  \item Kontraktion:\\
    Aus $\phi \lor \phi$ lässt sich $\phi$ ableiten.
  \item Assoziativität:\\
    Aus $\phi \lor(\psi \lor \xi)$ lässt sich
    $(\phi \lor \psi) \lor \xi$ ableiten
  \item Schnittregel:\\
    Aus $\phi \lor \psi$ und $\lnot \phi \lor \xi$ lässt sich
    $\psi \lor \xi$ ableiten.
  \item Instanziierung:\\
    Aus $\phi$ lässt sich $\phi\subst v\psi$ für jede Variable $v$ und
    jeden Term $\psi$ ableiten.
  \end{itemize}
\end{frame}


\begin{frame}{Erfüllbarkeit (SAT)}
  \begin{itemize}
  \item Beschreibung des SAT Problems
  \item NP-Vollständigkeit
  \item Normalformen
  \item Praktische Lösbarkeit
  \item Ansätze zur Lösung
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{SAT}
  Gegeben sei eine aussagenlogische Formel $\phi$.  Das
  \emph{Erfüllbarkeitsproblem (SAT)} ist die Frage nach einer
  erfüllenden Belegung: Gibt es ein $\eta$ mit
  \begin{displaymath}
    \models_\eta \phi?
  \end{displaymath}

  \emph{SAT} war das erste Problem, das als \emph{NP-vollständig}
  bewiesen wurde.
\end{frame}

\begin{frame}
  \frametitle{NP}
  Ein \emph{Prüfprogramm (Verifier)} für eine Sprache $\A$ ist ein
  Algorithmus $V$, für den gilt
  \begin{displaymath}
    \A = \{ w \mid V \text{ akzeptiert } \langle w, c \rangle 
    \text{ für eine Zeichenkette $c$} \}
  \end{displaymath}
  $c$ heißt \emph{Zertifikat} oder \emph{Beweis}.

  \emph{NP} ist die Klasse der Sprachen, die in polynomialer Zeit
  verifiziert werden können.  (Das heißt, dass Zertifikate maximal
  polynomiale Länge haben dürfen.)
\end{frame}

\begin{frame}
  \frametitle{NP-Vollständigkeit}
  Eine Sprache $B$ ist \emph{NP-vollständig}, wenn
  \begin{itemize}
  \item sie in \emph{NP} enthalten ist und
  \item jedes $A$ in \emph{NP} in polynomialer Zeit auf $B$ reduziert
    werden kann.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Cook-Levin Theorem}
  \emph{SAT} ist \emph{NP}-vollständig
  \begin{itemize}
  \item Beweisskizze: Tableau der zertifikatsprüfenden
    Turing-Maschine lässt sich in Aussagenlogik codieren
  \item Konsequenzen für Algorithmen:
    \begin{itemize}
    \item Wahrscheinlich gibt es keinen Algorithmus, der im
      schlechtesten Fall besser ist als Wahrheitstabellen
    \item In der Praxis lassen sich fast alle \emph{SAT}-Probleme gut
      lösen
    \end{itemize}

  \end{itemize}
\end{frame}

\begin{frame}{Normalformen}
  \begin{itemize}
  \item Warum Normalformen?
  \item Konjunktive Normalform (CNF)
  \item Disjunktive Normalform (DNF)
  \item \dots
  \item Algorithmus zur Umwandlung in CNF
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Warum Normalformen?}
  \begin{itemize}
  \item Einfacher für Computer
  \item Theoretische Erkenntnisse
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Konjunktive Normalform}
  \begin{itemize}
  \item Ein \emph{Literal} $L$ ist eine Variable $A$ oder eine
    negierte Variable $\lnot A$
    \begin{displaymath}
      L = A \mid \lnot A
    \end{displaymath}
  \item Eine Klausel $K$ ist eine Disjunktion von Literalen:
    \begin{displaymath}
      K = L_1 \lor L_2 \lor \dots \lor L_m
    \end{displaymath}
  \item Eine Formel $C$ ist in konjunktiver Normalform (KNF, CNF),
    wenn sie eine Konjunktion von Klauseln ist:
    \begin{align*}
      C &= K_1 \land K_2 \land \dots \land K_n\\
      &= (L_{11} \lor \dots \lor L_{1p}) \land \dots 
         \land (L_{k1} \lor \dots \lor L_{kp})
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Anwendung von CNF}
  \begin{itemize}
  \item Überprüfen von Allgemeingültigkeit
  \item Eine Klausel ist allgemeingültig genau dann, wenn sie Literale
    $L_i$, $L_j$ enthält, so dass $L_i = \lnot L_j$
  \item Eine Formel in CNF ist allgemeingültig, wenn jede ihrer
    Klauseln allgemeingültig ist
  \item Beispiel:
    \begin{displaymath}
      (A \lor B \lor \lnot C \lor \lnot B) 
      \land (B \lor \lnot C \lor E \lor F)
    \end{displaymath}
    ist nicht allgemeingültig, da die zweite Klausel nicht
    allgemeingültig ist
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Konvertierung in CNF}
  \begin{itemize}
  \item Gibt es zu jeder Formel $\phi$ eine äquivalente Formel $\psi$
    in CNF?
  \item Ja, aber $\psi$ ist nicht eindeutig:
  \item $\psi_1 = \phi$ und $\psi_2 = \phi \land (\phi \lor \chi) $
    sind beide CNF für $\phi$
  \item Die Entwicklung von Algorithmen, die ein $\psi$ mit möglichst
    geringen ``Kosten'' berechnen ist ein wichtiges Problem für
    formale Methoden
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithmus zur Konvertierung in CNF}
  \begin{itemize}
  \item Rekursion über die Struktur der Formel
    \begin{displaymath}
      \qtext{Wenn} \phi = \phi_1 \land \phi_2
      \qtext{dann} \psi = \psi_1 \land \psi_2
    \end{displaymath}
  \item Beweis der Korrektheit durch strukturelle Induktion
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithmus zur Konvertierung in CNF}
  \begin{itemize}
  \item Ersetze $\phi \iff \psi$ durch $\phi \implies \psi \land \psi \implies \phi$
  \item Ersetze $\phi \implies \psi$ durch $\lnot \phi \lor \psi$
  \item Schiebe Negationen nach innen; streiche doppelte Negation (Negationsnormalform)
  \item Dann: Ist $\phi$ ein Literal so ist $\psi$ gleich $\phi$
  \item Hat $\phi$ die Form $\phi_1 \land \phi_2$, so hat $\psi$ die
    Form $\psi_1 \land \psi_2$ wobei $\psi_1$ und $\psi_2$ rekursiv
    berechnet werden
  \item Hat $\phi$ die Form $\phi_1 \lor \phi_2$, so berechne $\psi$
    rekursiv als $\psi_1 \lor \psi_2$ und wende das Distributivgesetz
    an
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel}
  \begin{gather*}
    \phi \siff (\psi \lor \chi)\\
    \phi \imp (\psi \lor \chi) \land (\psi \lor \chi) \imp \phi\\
    (\lnot \phi \lor \psi \lor \chi) \land (\lnot (\psi \lor \chi) \lor \phi)\\
    (\lnot \phi \lor \psi \lor \chi) \land ((\lnot \psi \land \lnot\chi) \lor \phi)\\
    (\lnot \phi \lor \psi \lor \chi) \land (\lnot \psi  \lor \phi) \land (\lnot\chi  \lor \phi)\\
  \end{gather*}
\end{frame}

\begin{frame}{Praktische Lösungsverfahren}
  \begin{itemize}
  \item Analyse von Problemen: nur wenige sind schwer
  \item DPLL
  \item WalkSat
  \end{itemize}
\end{frame}

\begin{frame}{Implementierung eines DPLL-Solvers}
  \begin{itemize}
  \item Rekursiver Algorithmus
  \item Effizientere Implementierung
    \begin{itemize}
    \item Iterativer Algorithmus
    \item Heuristik zur Variablenauswahl
    \item Lernen von Klauseln
    \item Conflict-directed Backjumping
    \item Beobachtete Literale
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Rekursives DPLL}
  \begin{itemize}
  \item Grundidee: Rekursive Berechnung der Wahrheitstabelle
  \item Eingabe in CNF
  \item Frühzeitiger Abbruch
  \item ``Pure Symbol'' Heuristik
  \item ``Unit Clause'' Heuristik
  \end{itemize}
\end{frame}

\begin{frame}{Implementierung eines WalkSat-Solvers}
  \begin{itemize}
  \item Unvollständige Methode
  \item Random restarts
  \end{itemize}
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
