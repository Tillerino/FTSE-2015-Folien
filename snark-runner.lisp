;;; The following form serves to set up the ASDF system definition
;;; facility and the Snark theorem prover, so that this file can be
;;; compiled and loaded without any additional dependencies.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require :asdf)
  (asdf:load-system :snark)
  (require :snark))

(in-package "SNARK-USER")

;;; Utility Functions for Running Proofs.
;;; ====================================

;;; Printing Information About Proofs.
;;; ---------------------------------

(defparameter *print-proof-info* nil
  "Determines whether to print information about the proof.
The idea is to not output any additional information when directly
interacting with Snark, but to print information about the running
function when using a driver function like RUN to perform the
proofs.")

(defun print-n-answers (&optional function (n 5))
  "Print at most N answers obtained from Snark.

This function repeatedly calls (ANSWER) and (COLLECT) until (ANSWER)
returns NIL or N answers were obtained.  It then prints the obtained
answers.  If FUNCTION is non-NIL, it is called before collecting the
answers, so that a fresh proof state can be obtained.  If
*PRINT-PROOF-INFO* is non-NIL, the name of the function is printed
before it is called."
  (when function
    (when *print-proof-info*
      (format t "~2&Running ~A." function))
    (funcall function))
  (let ((result (loop for i from 1 to n
		   when (answer)
		   collect (if (eql (answer) 'snark-lisp::$$false)
			       :proof-found
			       (rest (answer)))
		   do (closure))))
    (loop for i from 1
       and answer in result
       do
	 (if (consp answer)
	     (format t "~&The ~:R answer is~,3T~{~A~^, ~}." i answer)
	     (format t "~&A proof was found.")))))

(defun prove* (form &rest args &key answer &allow-other-keys)
  "A simple wrapper around PROVE that prints information about FORM
before trying to prove it when *PRINT-PROOF-INFO* is true."
  (if *print-proof-info*
      (progn
	(format t "~&Trying to prove ~W with answer ~W."
		form answer)
	(apply 'prove form args))
      (apply 'prove form args)))


;;; A Simple Driver for Running Proofs.
;;; ----------------------------------

(defparameter *all-proof-functions* '()
  "A list of all names of 'proof-functions' that should be run, in
  reverse order.  A proof-function is a parameterless function that
  initializes a theory and runs a single PROVE form.")

(defun add-proof-function (name)
  "Add the function NAME to the list of all proof functions."
  (pushnew name *all-proof-functions*))

(defun run (&optional (proof-functions (reverse *all-proof-functions*)))
  "Run all proof functions in the list PROOF-FUNCTIONS (defaults to
*ALL-PROOF-FUNCTIONS* in the correct order."
  (let ((*print-proof-info* t))
    (mapcar 'print-n-answers proof-functions)))

(provide :snark-runner)
