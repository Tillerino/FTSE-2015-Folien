\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{synttree}
\usepackage{fancyvrb}

\input{Macros/macros.tex}

\title{Formale Techniken der Software-Entwicklung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}

\begin{frame}{Organisatorisches}
  \begin{itemize}
  \item Termine
    \begin{itemize}
    \item Vorlesung: Montag 12--15 Uhr
    \item Übung: Freitag 12-14 Uhr
    \end{itemize}
  \item Ort
    \begin{itemize}
    \item Richard-Wagner-Str. 10
    \item Raum 101
    \end{itemize}
  \item
    Personen
    \begin{itemize}
    \item Matthias Hölzl
    \item Christian Kroiß
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Einführung}\label{einfuxfchrung}

\begin{frame}{Formale Techniken}
  Verwendung von \emph{mathematischen Hilfsmitteln} zur
  \emph{Entwicklung} von Hardware und Software.
\end{frame}

\begin{frame}
  \frametitle{Warum Mathematik?}
  
  \smash{\hspace*{-10mm}\raisebox{-55mm}{\includegraphics[width=5cm]{Images/Gottfried_Wilhelm_Leibniz_c1700.jpg}}}

  \hspace*{4cm}
  \begin{minipage}{8cm}
    \begin{quotation}
      \noindent
      Das einzige Mittel, unsere Schlussfolgerungen zu verbessern,
      ist, sie ebenso anschaulich zu machen, wie es die der
      Mathematiker sind, derart, dass man seinen Irrtum mit den Augen
      findet und, wenn es Streitigkeiten unter Leuten gibt, man nur zu
      sagen braucht: Rechnen wir! ohne eine weitere Förmlichkeit, um
      zu sehen,
      wer recht hat.\\

      \hfill ---Gottfried Wilhelm Leibniz
    \end{quotation}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{Ist der Leibnizsche Traum realistisch?}
  \begin{beamerboxesrounded}{\centering \textbf{Nein}}
    \pause

    Physikalische System können nicht genau genug beschrieben
    werden:

    \begin{itemize}
    \item Ungenauigkeit von Messwerten
    \item Unsicherheit der physikalischen Gesetze
    \item Idealisierungen (z.B. Punktmasse)
    \item Schwierigkeit bei der Anwendung (Dreikörperproblem)
    \item Sensitive Abhängigkeit von Ausgangsbedingungen
    \end{itemize}
  \end{beamerboxesrounded}
    
  \pause\vspace*{1cm}
  \begin{beamerboxesrounded}{\centering \textbf{Nein}}
    Biologische oder psychologische Fragestellungen sind einer\\
    mathematischen Behandlung oft nur schwer zugänglich.
  \end{beamerboxesrounded}
\end{frame}

\begin{frame}
  \frametitle{Ist der Leibnizsche Traum realistisch?}
  \begin{beamerboxesrounded}{\centering \textbf{Aber}}
    \pause 

    Digitaltechnik versucht mathematische Modelle physikalisch zu\\
    realisieren!

    \begin{itemize}
    \item CPUs implementieren logische/arithmetische Operationen
    \item Computer sind endliche Automaten
    \item Programmiersprachen müssen einer exakten Grammatik folgen
    \item Die Semantik von Programmiersprachen wird durch
      Compiler/Interpreter realisiert
    \end{itemize}
  \end{beamerboxesrounded}
\end{frame}

\begin{frame}{Mathematische Hilfsmittel}
  \begin{itemize}
  \item \textbf{Logik}
  \item \textbf{Automatentheorie}
  \item Formale Sprachen
  \item Typsysteme
  \item Programmsemantik
  \item Wahrscheinlichkeitstheorie, Statistik
  \end{itemize}
  Die Mathematik beschäftigt sich oft mit Aussagen \emph{über} diese
  Formalismen.  Wir versuchen diese Techniken \emph{einzusetzen} um
  Software zu entwickeln.
\end{frame}

\begin{frame}{Entwicklung}
  \begin{itemize}
  \item Systemmodell/Implementierung
    \begin{itemize}
    \item Modell
      \begin{itemize}
      \item Ausführbar oder abstrakt
      \item Diskret, kontinuierlich oder hybrid
      \end{itemize}
    \item Implementierung
      \begin{itemize}
      \item Hardware oder ausführbares Programm
      \end{itemize}
    \end{itemize}
  \item Spezifikation
    \begin{itemize}
    \item Beschreibung einer Systemeigenschaft oder des
      Systemverhaltens
    \item Funktionale und nichtfunktionale Anforderungen
    \end{itemize}
  \item Verifikation
    \begin{itemize}
    \item Nachweis, dass Spezifikation von Modell oder Implementierung
      erfüllt wird
    \item Die Verifikation kann z.B. durch Tests informelle
      Argumentation, manuellen Beweis, ein automatisches Werkzeug
      (z.B. einen Model Checker) oder einen automatisierten Beweis
      erfolgen
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Geplanter Inhalt}
  \begin{itemize}
  \item ACL2 (A Computational Logic for Applicative
    Common Lisp)\\
    automatisches Theorembeweisen
  \item Aussagenlogik und Erfüllbarkeit\\
    SAT Solving
  \item Prädikatenlogik\\
    Resolutionsbeweisen
  \item Logiken höherer Ordnung\\
    interaktives Theorembeweisesn
  \item Temporallogiken und Automaten\\
    Model Checking
  \item Ausblick
  \end{itemize}
\end{frame}

\begin{frame}{Literatur}
  \begin{itemize}
  \item Kaufmann, Manolios, Moore: Computer-Aided Reasoning---An
    Approach
  \item Kaufmann, Manolios, Moore: Computer-Aided Reasoning---Case
    Studies
  \item Shankar, Owre, Rushby, Stringer-Calvert: PVS Prover Guide
  \item Huth, Ryan: Logic in Computer Science
  \end{itemize}
\end{frame}

\section{Modelle}
\label{sec:modelle}

\begin{frame}
  \frametitle{Modelle}
  Systemmodelle werden oft unterschieden in
  \begin{itemize}
  \item Funktionale (dynamische) Modelle
    \begin{itemize}
    \item Aktivitäten
    \item Prozesse
    \item Operationen
    \end{itemize}
  \item Architekturmodelle (statische Modelle)
    \begin{itemize}
    \item Dekomposition des Systems
    \item Schnittstellen
    \end{itemize}
  \item Im Moment: Systemmodelle und Spezifikationen in ACL2.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{ACL2}
  \begin{itemize}
  \item ACL2: A Computational Logic for Applicative Common Lisp
  \item Programmiersprache
  \item Logik
  \item Automatischer Theorembeweiser\\[3ex]
  \item ACL2-Modelle sind (meist) ausführbar
  \item Wir können die Modelle auch zur Simulation/zum Testen
    verwenden
  \item \url{http://www.cs.utexas.edu/users/moore/acl2/}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{ACL2} 

  ACL2 gehört zu den am meisten in der Industrie verwendeten
  Verifikationssystemen:
    \begin{itemize}
    \item Gleitkommaeinheiten der AMD Athlon und IBM Power
      Mikroprozessoren
    \item JVM: Speichermodell, Bytecode-Verifier
    \item Algorithmen, z.B. Boyer-Moore Suche in Strings
    \item Verifikation von LLVM Bytecode
    \item Verifikation von Kryptographie-Code
    \item ACL2 Beweischecker
    \item \dots
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{ACL2} \gray{ACL2: A Computational Logic for}
  \textbf{Applicative Common Lisp}
  \bigskip
  \begin{itemize}
  \item Basierend auf Common Lisp
  \item Nur rein funktionale Konstrukte
    \begin{itemize}
    \item Kein veränderbarer Zustand
    \item Keine globalen Variablen
    \end{itemize}
  \item Keine Funktionen höherer Ordnung
  \item Eingeschränkte Syntax von Funktionen
  \item Eingeschränkte Metaprogrammierung
  \item Kein Objektsystem
  \item Erweiterungen um funktionale Programmierung praktikabel zu
    machen
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{ACL2} 

  \gray{ACL2: A} \textbf{Computational Logic} \gray{for Applicative
    Common Lisp}
  \bigskip
  
  \begin{itemize}
  \item Programme haben Interpretation als logische Aussagen
  \item Quantorenfreie Prädikatenlogik
  \item Erweiterungen für Skolemisierung, etc.
  \item Induktion
  \item Entscheidungsprozeduren (z.B. lineare Arithmetik)
  \item Automatischer Theorembeweiser
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{ACL2 Sedan}
  \vspace*{-1cm}
  \begin{center}
    \includegraphics[width=11cm]{Images/acl2s-screenshot.png}
  \end{center}
  \url{http://acl2s.ccs.neu.edu/acl2s/doc/}
\end{frame}

\begin{frame}
  \frametitle{ACL2 Sedan}

  \hspace*{-8mm}
  \raisebox{4mm}{\includegraphics[width=3cm]{Images/acl2-logo.jpg}}
  \raisebox{10mm}{\huge{$+$}}
  \includegraphics[width=3cm]{Images/Emacs-logo.png}
  \raisebox{10mm}{\huge{$=$}}
  \includegraphics[width=4cm]{Images/alonso-2010.jpg}

  \bigskip
  \hspace*{25mm}(Für erfahrene Anwender)
\end{frame}

\begin{frame}
  \frametitle{ACL2 Sedan}

  \hspace*{-8mm}
  \raisebox{4mm}{\includegraphics[width=3cm]{Images/acl2-logo.jpg}}
  \raisebox{10mm}{\huge{$+$}}
  \includegraphics[width=3cm]{Images/Emacs-logo.png}
  \raisebox{10mm}{\huge{$=$}}
  \includegraphics[width=4cm]{Images/car-crash}

  \bigskip
  \hspace*{35mm}(Für Anfänger)
\end{frame}

\begin{frame}
  \frametitle{ACL2 Sedan}

  \raisebox{4mm}{\includegraphics[width=5cm]{Images/acl2-logo.jpg}}
  \raisebox{10mm}{\huge{$\sim$}}
  \includegraphics[width=5cm]{Images/Jules_Bianchi_Bahrein_2014.jpg}

  \raisebox{-4mm}{\includegraphics[width=5cm]{Images/acl2s.jpg}}
  \raisebox{10mm}{\huge{$\sim$}}
  \includegraphics[width=5cm]{Images/sedan}
\end{frame}


\begin{frame}
  \frametitle{ACL2 Sedan}
  \vspace*{-1cm}
  \begin{center}
    \includegraphics[width=11cm]{Images/acl2s-screenshot.png}
  \end{center}
  \url{http://acl2s.ccs.neu.edu/acl2s/doc/}
\end{frame}


\section{ACL2 als Programmiersprache}
\label{sec:acl2-prog}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Datentypen}
  \begin{itemize}
  \item Zahlen: \texttt{0}, \texttt{-123}, \texttt{82/3},
    \texttt{\#c(2, 7/3)}
  \item Zeichen: \verb|#\A|, \verb|#\a|, \verb|#\Space|,
    \verb|#\Newline| % | 
  \item Zeichenketten: \verb|"Strings"|, \verb|""| % |
  \item Symbole: \verb|nil|, \verb|t|, \verb|this-is-a-symbol|
  \item Cons-Zellen: \verb|(1 . 2)|,\\
    \hspace*{21mm}\verb|((a . 1) . (b . 2))|,\\
    \hspace*{21mm}\verb|(1 . (2 . (3 . nil)))|,\\
    \hspace*{21mm}\verb|(1 2 3)|
  \item Benutzerdefinierte Datentypen, lineare Objekte
    (\texttt{stobj}s), \dots
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Atome (1)}
  \begin{itemize}
  \item Nur rationale Zahlen, keine Gleitkomma-Darstellung
  \item Binäre, oktale und hexadezimale Darstellung:\\
    \verb|#b1101| $=$ \verb|#o15| $=$ \verb|#xd| $=$ \verb|13|
  \item Zeichen und Zeichenketten unterscheiden zwischen Groß- und
    Kleinschreibung
  \item Symbole werden beim Einlesen in Großbuchstaben umgewandelt:
    \verb|foo| $=$ \verb|Foo| $=$ \verb|FOO|
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Atome (2)}
  \begin{itemize}
  \item Symbole werden typischerweise klein geschrieben;\\
    Wörter werden mit Bindestrichen getrennt: \verb|i-am-a-symbol|
  \item Symbole können auch Sonderzeichen enthalten: \verb|really?|,
    \verb|+|
  \item Jedes Symbol gehört zu einem Paket; das Paket kann durch
    \verb|::| getrennt vor das Symbol geschrieben
    werden: \verb|a-package::my-symbol|, \verb|my-math::*|
  \item Am Beginn einer ACL2-Session ist das aktuelle Paket
    \verb|ACL2|, d.h., \verb|cons| steht für \verb|acl2::cons|
  \item \verb|t| und \verb|nil| sind Symbole, die auch die
    Wahrheitswerte repräsentieren (\verb|nil| auch die leere Liste)
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Programmieren mit ACL2: Cons-Zellen (1)}
    \begin{itemize}
    \item Eine Cons-Zelle ist ein geordnetes Paar von Objekten:
      \verb|(1 . 2)| ist das Paar aus den Zahlen \texttt{1} und
      \texttt{2}
    \item Cons-Zellen können beliebig verschachtelt werden um
      Binärbäume zu repräsentieren:
      \verb|((1 . 2) . (3 . 4))| ist der Baum\\
      \hspace*{2cm}\synttree[$\cdot$[$\cdot$[1][2]][$\cdot$[3][4]]]
    \item Das erste Element einer Cons-Zelle heißt ihr \texttt{car},
      das zweite ihr \texttt{cdr}
    \item \verb|()| $=$ \verb|nil| ist der leere Baum
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Cons-Zellen (2)}
  \begin{itemize}
  \item Statt \verb|(1 . nil)| kann man \verb|(1)| schreiben\\
    statt\verb|(1 . (2 . nil))| kann man \verb|(1 2)| schreiben
  \item Listen werden also durch spezielle Bäume repräsentiert:
    \verb|(1 2 3)| ist\\
    \hspace*{3cm}\synttree[$\cdot$[\texttt{1}][$\cdot$[\texttt{2}][$\cdot$[\texttt{3}][\texttt{nil}]]]]
  \item Die Elemente der Liste sind die Blätter des Baumes, die links
    stehen (\texttt{1}, \texttt{2} und \texttt{3})
  \end{itemize}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Listen}
  \begin{itemize}
  \item Listen können beliebig verschachtelt werden
  \item Die Liste\\[3mm]
    \quad \verb|((1 2 3) (3 (4 (5 6))) 7 eight)|\\[3mm]
    hat 4 Elemente:
    \begin{itemize}
    \item die Liste \verb|(1 2 3)|
    \item die Liste \verb|(3 (4 (5 6)))|
    \item die Zahl \verb|7|
    \item das Symbol \verb|eight|
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Programme}

  \begin{itemize}
  \item ACL2 Programme werden durch Listen repräsentiert
  \item Symbole stehen für Variablen (und Funktionen)
  \item Listen stehen für Funktionsaufrufe, und spezielle Formen wie
    Kontrollstrukturen oder Definitionen

    \begin{Acl2}
      (+ 1 2)              \eval 3
      (+ (* 2 3) (* 4 5))  \eval 26
      (+ 1 2 3)            \eval 6

      (if t 
          1 
          2)               \eval 1
      (if nil 1 2)         \eval 2
    \end{Acl2}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Listen/Quotes} 

  Damit sie nicht als Funktionsaufrufe interpretiert werden müssen
  Listen-Literale mit \verb|'| vor Auswertung geschützt (``gequotet'')
  werden

  \begin{Acl2}
    (car '(1 . 2)) \eval 1
    (cdr '(1 . 2)) \eval 2
  \end{Acl2}
  Dabei genügt es, die äußerste Form zu schützen, keine der Teilformen
  wird dann ausgewertet:
  \begin{Acl2}
    '(expt (+ 1 (* 2 3)) (car x)) 
               \eval (expt (+ 1 (* 2 3)) (car x))
  \end{Acl2}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Programmieren mit ACL2: Definitionen}
  \begin{itemize}
  \item Mit \texttt{defun} werden neue Funktionen definiert:
    \begin{Acl2}[gobble=6]

      (defun add-42 (x)
        (+ x 42))        \verbsfit{Definiert Funktion }add-42
      (add-42 1)         \eval 43
    \end{Acl2}
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
