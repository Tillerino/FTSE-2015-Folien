\documentclass[ignorenonframetext]{beamer}

\usecolortheme{crane}

\usepackage{amssymb,amsmath}
\usepackage[utf8]{inputenc}
\usepackage[german]{babel}
\usepackage{turnstile}
\usepackage{fancyvrb}
%\usepackage{manfnt}
\usepackage{qtree}

\input{Macros/macros.tex}

\title{Formale Techniken der Software-Entwicklung}
\author{Matthias Hölzl, Christian Kroiß}

\begin{document}
\frame{\titlepage}
\begin{frame}
  \frametitle{Beispiele}
  Die folgenden Beispiele sind alle im Ordner \texttt{Lecture-05} im
  GitLab Repository:
  \begin{itemize}
  \item \texttt{simple-examples.lisp}
  \item \texttt{fact.lisp}
  \item \texttt{ac-example.lisp}
  \item \texttt{insertion-sort.lisp}
  \item \texttt{tree.lisp}
  \item \texttt{adder.lisp}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Binärarithmetik: Volladdierer}
  \begin{center}
    \begin{tabular}{l|l|l||l|l}
      \texttt{p}&\texttt{q}&\texttt{c}&\textit{sum}&\textit{carry}\\
      \hline
      0 & 0 & 0 & 0 & 0\\
      1 & 0 & 0 & 1 & 0\\
      0 & 1 & 0 & 1 & 0\\
      0 & 0 & 1 & 1 & 0\\
      1 & 1 & 0 & 0 & 1\\
      1 & 0 & 1 & 0 & 1\\
      0 & 1 & 1 & 0 & 1\\
      1 & 1 & 1 & 1 & 1\\
    \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Binärarithmetik: Volladdierer}
  \begin{center}
    \includegraphics[width=11cm]{Images/full-adder}
  \end{center}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Binärarithmetik: Volladdierer}
\begin{Verbatim}
(defun full-adder (p q c)
  (mv (bxor p (bxor q c))
      (bmaj p q c)))
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Zurück zu SAT}
  Gegeben sei eine aussagenlogische Formel $\phi$.  Das
  \emph{Erfüllbarkeitsproblem (SAT)} ist die Frage nach einer
  erfüllenden Belegung: Gibt es ein $\eta$ mit
  \begin{displaymath}
    \models_\eta \phi?
  \end{displaymath}

  \emph{SAT} war das erste Problem, das als \emph{NP-vollständig}
  bewiesen wurde.
\end{frame}

\begin{frame}
  \frametitle{NP}
  Ein \emph{Prüfprogramm (Verifier)} für eine Sprache $\A$ ist ein
  Algorithmus $V$, für den gilt
  \begin{displaymath}
    \A = \{ w \mid V \text{ akzeptiert } \langle w, c \rangle 
    \text{ für eine Zeichenkette $c$} \}
  \end{displaymath}
  $c$ heißt \emph{Zertifikat} oder \emph{Beweis}.

  \emph{NP} ist die Klasse der Sprachen, die in polynomialer Zeit
  verifiziert werden können.  (Das heißt, dass Zertifikate maximal
  polynomiale Länge haben dürfen.)
\end{frame}

\begin{frame}
  \frametitle{NP-Vollständigkeit}
  Eine Sprache $B$ ist \emph{NP-vollständig}, wenn
  \begin{itemize}
  \item sie in \emph{NP} enthalten ist und
  \item jedes $A$ in \emph{NP} in polynomialer Zeit auf $B$ reduziert
    werden kann.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Cook-Levin Theorem}
  \emph{SAT} ist \emph{NP}-vollständig
  \begin{itemize}
  \item Beweisskizze: Tableau der zertifikatsprüfenden
    Turing-Maschine lässt sich in Aussagenlogik codieren
  \item Konsequenzen für Algorithmen:
    \begin{itemize}
    \item Wahrscheinlich gibt es keinen Algorithmus, der im
      schlechtesten Fall besser ist als Wahrheitstabellen
    \item In der Praxis lassen sich fast alle \emph{SAT}-Probleme gut
      lösen
    \end{itemize}

  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{SAT durch Wahrheitstabellen}

  Eine Formel $\phi$ ist \emph{erfüllbar} (satisfiable), wenn sie für
  mindestens eine Belegung wahr ist, wenn also gilt

  \begin{displaymath}
    \exists \eta: \A \to \Bool: \sem{\phi}\eta = \true
  \end{displaymath}


  Eine Formel $\phi$ ist genau dann erfüllbar, wenn in der letzten
  Spalte ihrer Wahrheitstabelle mindestens einmal der Wert $\true$
  vorkommt.

  \begin{displaymath}
    \begin{array}{c|c}
      \cdots & \phi\\
      \hline
      \cdots & \vdots\\
      \cdots & \true\\
      \cdots & \vdots\\
    \end{array}
  \end{displaymath}
\end{frame}

\begin{frame}[fragile]
  \frametitle{SAT durch Wahrheitstabellen (ACL2)}
  Wir haben bereits eine ACL2-Funktion implementiert, die (eine
  Variante von) SAT durch Wahrheitstabellen implementiert:
\begin{Verbatim}
(defun model-check-aux (term envs)
  (if (endp envs)
    '()
    (cons (eval-bool term (first envs))
          (model-check-aux term (rest envs)))))

(defun model-check (term)
  (let ((results (model-check-aux
                     term
                     (generate-envs (vars term)))))
    (cond ((all-true results) :valid)
          ((any-true results) :satisfiable)
          (t                  :insatisfiable))))
  \end{Verbatim}
\end{frame}

\begin{frame}{Praktische Lösungsverfahren}
  Das bisher betrachtete Verfahren \emph{SAT}-Probleme mit
  Wahrheitstabellen zu lösen ist nur für sehr kleine Probleme
  praktisch durchführbar.

  Obwohl es aufgrund der NP-Vollständigkeit von \emph{SAT}
  (wahrscheinlich) kein effizientes Verfahren gibt um \emph{alle}
  \emph{SAT} Probleme zu lösen, gibt es Verfahren, die \emph{fast
    alle} \emph{SAT}-Probleme viel effizienter lösen als die
  Berechnung der vollständigen Wahrheitstabelle.

  Verfahren wie DPLL oder WalkSat können fast alle (und insbesondere
  fast alle praktisch auftretenden) \emph{SAT}-Probleme gut lösen.
  Nur Probleme mit einer besonderen Struktur bzw. einem bestimmten
  Verhältnis von Klauseln zu Variablen sind für diese Algorithmen
  problematisch.  Im Folgenden werden wir einige der Techniken
  betrachten, die dafür eingesetzt werden.
\end{frame}

\begin{frame}
  \frametitle{Backtracking-Suche}
  \begin{itemize}
  \item Zustandsraum: alle möglichen (partiellen) Belegungen der Variablen
  \item Rekursive Aufzählung des Zustandsraumes
  \item Faktorisierte Darstellung der Zustände: $\{A_1 = \eta_1,
    \dots, A_k = \eta_k\}$
  \item Suchoperatoren: Hinzufügen einer neuen Variablenbelegung
    \begin{align*}
      \{A_1 = \eta_1, \dots, A_k = \eta_k\} &\to\\
      \{A_1 = \eta_1, \dots, A_k &= \eta_k, A_{k+1} = \eta_{k+1}\}
    \end{align*}
  \item Feste Ordnung der Variablen
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Backtracking-Suche}
  Bei der "`naiven"' Backtracking-Suche wird der komplette
  Zustandsraum durchlaufen:

  \Tree [.$A_1$ [.$A_2$ [.$A_3$ [.$A_4$ {$\eta_1$} ] [.$A_4$ {$\eta_2$} ] ]
                        [.$A_3$ [.$A_4$ {$\eta_3$} ] [.$A_4$ {$\eta_4$} ] ] ]
                [.$A_2$ [.$A_3$ [.$A_4$ {$\eta_5$} ] [.$A_4$ {$\eta_6$} ] ]
                        [.$A_3$ [.$A_4$ {$\eta_7$} ] [.$A_4$ {$\eta_8$} ] ] ] ]
\end{frame}

\begin{frame}[fragile,fragile]
  \frametitle{Implementierung}
\begin{Verbatim}
(defun bt-sat (term env open-vars)
  (if (endp open-vars)
    (eval-bool term env)
    (or (bt-sat term
                (cons (cons (first open-vars) t) env)
                (rest open-vars))
        (bt-sat term
                (cons (cons (first open-vars) nil) env)
                (rest open-vars)))))
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Verbesserung: Frühzeitiger Abbruch der Suche}
  Die erste Implementierung der Backtracking-Suche erzeugt immer eine
  vollständige Belegung der Variablen und wertet dann den Term aus.
  
  In vielen Fällen kann man die Suche schon früher abbrechen:
  \begin{gather*}
    \blue{A \lor \lnot A} \lor (B \land C) \lor (A \land B)\\
    (\blue{A} \lor B) \blue{\land} (\blue{\lnot A} \land C \land D) \land \lnot B
  \end{gather*}
  Beim ersten Term ist schon nach der Belegung von $A$ durch
  einen beliebigen Wert klar, dass der Term wahr ist; beim zweiten
  Term kann man nach der Wahl von $A$ und $B$ schon feststellen, dass
  kein Wert von $C$ oder $D$ den Term noch erfüllen kann.
\end{frame}

\begin{frame}
  \frametitle{Auswertung bei partieller Belegung}
  Um mit partiellen Variablenbelegungen umgehen zu können kann man die
  Wahrheitstabellen folgendermaßen erweitern:
  \begin{gather*}
    \begin{array}{c|c}
      & \lnot\\
      \hline
      \false & \true \\
      \true  & \false\\
      \undef & \undef
    \end{array}\\[3ex]
    \begin{array}{c|c|c|c}
      \land  & \false & \true  & \undef \\
      \hline
      \false & \false & \false & \false\\
      \true  & \false & \true  & \undef\\
      \undef & \false & \undef & \undef
    \end{array}
    \qquad
    \begin{array}{c|c|c|c}
      \lor  & \false  & \true & \undef\\
      \hline
      \false & \false & \true & \undef\\
      \true  & \true  & \true & \true\\
      \undef & \undef & \true & \undef
    \end{array}
  \end{gather*}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Auswertung bei partieller Belegung}
\begin{Verbatim}
(defun eval-bool3 (exp env)
  (cond ((symbolp exp)
         (if (or (equal exp t) (equal exp nil))
           exp
           (lookup3 exp env)))
        ((atom exp) 'undefined)
        ((equal (length exp) 2)
         (not3 (eval-bool3 (second exp) env)))
        ...
\end{Verbatim}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Auswertung bei partieller Belegung}
\begin{Verbatim}
        ...
        ((equal (length exp) 3)
         (let ((lhs (eval-bool3 (second exp) env))
               (rhs (eval-bool3 (third exp) env)))
           (case (first exp)
                 (or        (or3      lhs rhs))
                 (and       (and3     lhs rhs))
                 (implies   (implies3 lhs rhs))
                 (iff       (iff3     lhs rhs))
                 (xor       (xor3     lhs rhs))
                 (otherwise 'undefined))))
        (t 'undefined)))
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Effizientere Backtracking-Suche}
  Damit kann man in vielen Fällen die Suche schon früher abbrechen:

  \hspace*{1cm}
  \Tree [.$A_1$ [.$A_2$ [.$A_3$ [.$A_4$ {$\eta_1$} ] [.$A_4$ {$\eta_2$} ] ]
                        [.$A_3$ {$\blue{\eta_3 = \eta_4}$} ] ]
                [.$A_2$ [.$A_3$ {$\blue{\eta_5 = \eta_6}$} ]
                        [.$A_3$ [.$A_4$ {$\eta_7$} ] [.$A_4$ {$\eta_8$} ] ] ] ]
  \vspace*{3mm}

  Das ist in der Funktion \texttt{bt-sat3} implementiert, die den
  Wahrheitswert von Termen mit \texttt{eval-bool3} auswertet.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Effizientere Backtracking-Suche}
\begin{Verbatim}
(defun bt-sat3 (term env open-vars)
  (declare (xargs :measure (acl2-count open-vars)))
  (let ((result (eval-bool3 term env)))
    (if (or (not (equal result 'undefined))
            (endp open-vars))
      result
      (or (bt-sat3 term
                   (cons (cons (first open-vars) t)
                          env)
                   (rest open-vars))
          (bt-sat3 term
                   (cons (cons (first open-vars) nil) 
                         env)
                   (rest open-vars))))))
\end{Verbatim}
\end{frame}

\begin{frame}
  \frametitle{Verwendung von CNF}
  \begin{itemize}
  \item Der bisherige Algorithmus ist für beliebige Formeln verwendbar
  \item Die Auswertung ist immer noch nicht effizient
  \item Für weitere Verbesserungen benötigen wir mehr Information über
    die Struktur der Terme
  \item Diese Struktur bekommen wir z.B. durch CNF
  \item Keine Änderung am Algorithmus \emph{nötig}, sofern wir in CNF
    das Interface implementieren, das zum Backtracking benötigt wird
  \item Viele Verbesserungen \emph{möglich}, wenn wir auf CNF
    umsteigen
  \item Beispiel: Propagation
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Propagation}
  Bei der Propagation wird der Suchraum verkleinert, indem wir
  die Struktur des Problems oder der Lösung verwenden, um gewisse
  Zweige des Suchbaums auszuschließen:

  \hspace*{-5mm}
  {\footnotesize
  \Tree [.$A_1$ [.$A_2$ [.$A_3$ [.$\blue{A_4}$ {$\blue{\eta_1}$} ] 
                               [.$\blue{A_4}$ {$\blue{\eta_2}$} ] ]
                        [.$A_3$ {$\eta_3 = \eta_4$} ] ]
                [.$A_2$ [.$A_3$ {$\eta_5 = \eta_6$} ]
                        [.$A_3$ [.$A_4$ {$\eta_7$} ] [.$A_4$
                        {$\eta_8$} ] ] ] ]
                      $\to$
  \Tree [.$A_1$ [.$A_2$ [.$A_3$ [.$\blue{A_4}$ {$\blue{\eta_2}$} ] ]
                        [.$A_3$ {$\eta_3 = \eta_4$} ] ]
                [.$A_2$ [.$A_3$ {$\eta_5 = \eta_6$} ]
                        [.$A_3$ [.$A_4$ {$\eta_7$} ] [.$A_4$
                        {$\eta_8$} ] ] ] ]
  }
\end{frame}

\begin{frame}
  \frametitle{Propagation}
  Beispiel: Wenn eine Klausel nur ein Literal $L$ enthält, so muss die
  entsprechende Variable $A$ \texttt{true} oder \texttt{false} sein,
  je nachdem ob $L$ positiv (nicht negiert, d.h. $L \equiv A$) oder
  negativ (negiert, d.h. $L \equiv \lnot A$) ist.

  Ein solches Literal kann man dann aus anderen Klauseln entfernen;
  wir werden das später bei der Unit-Resolutionsregel noch genauer
  betrachten.
\end{frame}

\begin{frame}{DPLL-Algorithmus}
  \begin{itemize}
  \item Basis: Backtracking-Algorithmus
  \item Effizientere Implementierung
    \begin{itemize}
    \item \blue{Frühzeitiger Abbruch}
    \item Elimination von reinen Literalen (pure symbol heuristic,
      pure literal elimination)
    \item \blue{Unit-Propagation (unit clause heuristic)}
    \end{itemize}
  \item Noch Effizientere Implementierung
    \begin{itemize}
    \item Iterativer Algorithmus
    \item Heuristik zur Variablenauswahl
    \item Conflict-directed Backjumping
    \item Lernen von Klauseln
    \item Beobachtete Literale
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Normalformen}
  Es gibt viele Normalformen:
  \begin{itemize}
  \item Negationsnormalform (NNF)
  \item Konjunktive Normalform (CNF)
  \item Disjunktive Normalform (DNF)
  \item \dots
  \end{itemize}
  Wir betrachten einen Algorithmus zur Umwandlung in CNF, der als Teil
  der Umwandlung auch eine NNF erzeugt.
\end{frame}

\begin{frame}
  \frametitle{Konjunktive Normalform}
  \begin{itemize}
  \item Ein \emph{Literal} $L$ ist eine Variable $A$ oder
    eine negierte Variable $\lnot A$
    \begin{displaymath}
      L = A \mid \lnot A
    \end{displaymath}
    Wir bezeichnen Literale mit $L$, $M$ oder $N$.
  \item Eine Klausel $K$ ist eine Disjunktion von Literalen:
    \begin{displaymath}
      K = L_1 \lor L_2 \lor \dots \lor L_m
    \end{displaymath}
  \item Eine Formel $C$ ist in konjunktiver Normalform (KNF, CNF),
    wenn sie eine Konjunktion von Klauseln ist:
    \begin{align*}
      C &= K_1 \land K_2 \land \dots \land K_n\\
      &= (L_1 \lor \dots \lor L_p) \land \dots 
         \land (M_1 \lor \dots \lor M_q)
    \end{align*}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Konvertierung in CNF}
  \begin{itemize}
  \item Gibt es zu jeder Formel $\phi$ eine äquivalente Formel $\cnf(\phi)$
    in CNF?
  \item Ja, aber $\cnf(\phi)$ ist nicht eindeutig:
  \item $\psi_1 = \phi$ und $\psi_2 = \phi \land (\phi \lor \chi) $
    sind beide CNF für $\phi$
  \item Die Entwicklung von Algorithmen, die ein $\cnf(\phi)$ mit
    möglichst geringen ``Kosten'' berechnen ist ein wichtiges Problem
    für formale Methoden
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Algorithmus zur Konvertierung in CNF}
  \begin{itemize}
  \item Ersetze $\phi_1 \iff \phi_2$ durch
    $(\phi_1 \implies \phi_2) \land (\phi_2 \implies \phi_1)$
  \item Ersetze $\phi_1 \implies \phi_2$ durch $\lnot \phi_1 \lor \phi_2$
  \item Schiebe Negationen nach innen; streiche doppelte Negation (Negationsnormalform)
  \item Dann: Ist $\phi$ ein Literal so ist $\cnf(\phi)$ gleich $\phi$
  \item Hat $\phi$ die Form $\phi_1 \land \phi_2$, so hat $\cnf(\phi)$
    die Form $\cnf(\phi_1) \land \cnf(\phi_2)$ wobei $\cnf(\phi_1)$
    und $\cnf(\phi_2)$ rekursiv berechnet werden
  \item Hat $\phi$ die Form $\phi_1 \lor \phi_2$, so berechne rekursiv
    $\cnf(\phi_1)$ und $\cnf(\phi_2)$.
    \begin{itemize}
    \item Enthält weder $\cnf(\phi_1)$ noch $\cnf(\phi_2)$ eine
      Konjunktion, so ist $\cnf(\phi_1) \lor \cnf(\phi_2)$ eine
      Klausel.
    \item Enthält ein $\phi_i$ eine Konjunktion, so ist der äußerste
      Operator von $\cnf(\phi_i)$ ebenfalls eine Konjunktion.  Wende
      so oft das Distributivgesetz auf
      $\cnf(\phi_1) \lor \cnf(\phi_2)$ an, bis alle $\land$ über allen
      $\lor$ stehen.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Beispiel}
  \begin{gather*}
    A \siff (B \lor C)\\
    A \imp (B \lor C) \land (B \lor C) \imp A\\
    (\lnot A \lor B \lor C) \land (\lnot (B \lor C) \lor A)\\
    (\lnot A \lor B \lor C) \land ((\lnot B \land \lnot C) \lor A)\\
    (\lnot A \lor B \lor C) \land (\lnot B  \lor A) \land (\lnot C  \lor A)\\
  \end{gather*}
\end{frame}


\begin{frame}[fragile]
  \frametitle{CNF In ACL2}
  Siehe ACL2-Session.
\end{frame}

\begin{frame}
  \frametitle{Anwendung von CNF: Allgemeingültigkeit}
  \begin{itemize}
  \item Überprüfen von Allgemeingültigkeit
  \item Eine Klausel ist allgemeingültig genau dann, wenn sie
    komplementäre Literale $L_i$, $L_j$ enthält, d.h., $L_i = \lnot
    L_j$ oder $L_j = \lnot L_i$
  \item Eine Formel in CNF ist allgemeingültig, wenn jede ihrer
    Klauseln allgemeingültig ist
  \item Beispiel:
    \begin{displaymath}
      (L_1 \lor L_2 \lor \lnot L_3 \lor \lnot L_2) 
      \land (L_2 \lor \lnot L_3 \lor L_4 \lor L_5)
    \end{displaymath}
    ist nicht allgemeingültig, da die zweite Klausel nicht
    allgemeingültig ist
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Schreibweise}
  Formeln in CNF werden oft in Mengenschreibweise notiert.
  Statt
  \begin{displaymath}
    (L_1 \lor L_2 \lor \lnot L_3 \lor \lnot L_2) 
    \land (L_2 \lor \lnot L_3 \lor L_4 \lor L_5)
  \end{displaymath}
  schreibt man dann
  \begin{displaymath}
    \{\{L_1, L_2, \lnot L_3, \lnot L_2\}, \{L_2, \lnot L_3, L_4, L_5\}\}
  \end{displaymath}
  oder auch nur
  \begin{displaymath}
    L_1, L_2, \lnot L_3, \lnot L_2 \qquad L_2, \lnot L_3, L_4, L_5
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Anwendung von CNF: Inkonsistenz}
  \begin{itemize}
  \item Überprüfen von Inkonsistenz
  \item Eine Formel in CNF ist genau dann inkonsistent, wenn die leere
    Klausel ableitbar ist
  \item Beispiel:
    \begin{displaymath}
      \{\{L_1, L_2, \lnot L_3, \lnot L_2\}, \emptyset, \{L_2, \lnot L_3, L_4, L_5\}\}
    \end{displaymath}
    ist inkonsistent, da eine leere Klausel enthalten ist
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Unit-Resolution}
  Beim DPLL-Algorithmus haben wir die folgende Ableitungsregel für
  Klauseln betrachtet: Wenn $M$ und $L_i$ komplementäre Literale sind,
  dann gilt
  \begin{displaymath}
    \infer{L_1, \dots, L_m \qquad M}{L_1, \dots, L_{i-1}, L_{i+1},
      \dots, L_m}
  \end{displaymath}
  Diese Regel bezeichnet man als (Unit-Resolution).

  (Dass diese Regel korrekt ist überlegt man sich so: Da $M$ und $L_i$
  komplementär sind kann man die linke Seite der Prämisse als
  \begin{displaymath}
    M \imp L_1, \dots, L_{i-1}, L_{i+1}, \dots, L_m
  \end{displaymath}
  schreiben, und aus $M$ und $M \imp \phi$ folgt $\phi$.)
\end{frame}

\begin{frame}
  \frametitle{Beispiel}
  $L_1 \imp L_2$ und $L_2 \imp L_3$ haben die CNF $\lnot L_1, L_2$ bzw. $\lnot L_2,
  L_3$.  Somit gilt die Ableitung
  \begin{displaymath}
    \infer{\infer{\lnot L_1, L_2\qquad L_1}{L_2}\qquad \lnot L_2, L_3}{L_3}
  \end{displaymath}
\end{frame}

\begin{frame}
  \frametitle{Resolution}
  Die Beweisregel zur Unit-Resolution lässt sich folgendermaßen
  verallgemeinern: Falls $L_i$ und $M_j$ komplementär sind (d.h. $L_i
  \equiv \lnot M_j$ oder $\lnot L_i \equiv M_j$) gilt
  \begin{displaymath}
    \infer{L_1, \dots, L_m \qquad M_1, \dots, M_n}{L_1, \dots, L_{i-1}, L_{i+1},
      \dots, L_m, M_1 \dots, M_{j-1}, M_{j+1}, M_n}
  \end{displaymath}
  Diese Form der Ableitung heißt (allgemeine) Resolution.  $L_1,
  \dots, L_{i-1}, L_{i+1}, \dots, L_m, M_1 \dots, M_{j-1}, M_{j+1},
  M_n$ heißt \emph{Resolvente}.

  Die hier angegebene Resolutionsregel arbeitet immer auf Klauseln.
  Falls $L_1, \dots, L_m$ und $M_1, \dots, M_n$ keine komplementären
  Literale haben, so kann die Resolutionsregel nicht angewendet
  werden.
\end{frame}

\begin{frame}
  \frametitle{Resolution: Korrektheit}
  $L_i$, $M_j$ komplementär ($L_i \equiv \lnot M_j$ oder $\lnot L_i
  \equiv M_j$):
  \begin{displaymath}
    \infer{L_1, \dots, L_m \qquad M_1, \dots, M_n}{L_1, \dots, L_{i-1}, L_{i+1},
      \dots, L_m, M_1 \dots, M_{j-1}, M_{j+1}, M_n}
  \end{displaymath}

  Die Resolvente ist \emph{nicht} äquivalent zu den Ausgansklauseln.
  Sei z.B. $\eta$ eine Belegung in der nur $L_1$ wahr ist.  Dann ist
  die Resolvente wahr aber die Prämisse $M_1, \dots, M_n$ falsch.

  Aber: $L_1, \dots, L_m$ und $M_1, \dots, M_n$ sind nur dann beide
  erfüllbar, wenn die Resolvente erfüllbar ist; wenn also $L_1, \dots,
  L_m$ und $M_1, \dots, M_n$ beide wahr sind, so ist auch die
  Resolvente wahr.

  (Das kann man sich so überlegen: Sei $L_i$ wahr.  Dann ist $M_j$
  falsch; damit $M_1, \dots M_n$ gilt, musss also $M_1 \dots, M_{j-1},
  M_{j+1}, M_n$ gelten.  Falls $L_i$ falsch ist, kann man entsprechend
  mit $L_1, \dots, L_m$ argumentieren.)
\end{frame}

\begin{frame}
  \frametitle{Resolution: Korrektheit}
  Seien $C_1, C_2$ Formeln in CNF.  Dann haben $C_1$ und $C_2$ die
  Form $C_1 = K_1 \land \dots \land K_m$, $C_2 = K'_1 \land \dots
  \land K'_n$ mit Klauseln $K_i, K'_j$.  

  Wir sagen $C_2$ ist durch (Vorwärts)-Resolution aus $C_1$ ableitbar
  und schreiben
  \begin{displaymath}
    C_1 \derivesresfwd C_2
  \end{displaymath}
  wenn es für jede Klausel $K'_j$ einen Ableitungsbaum gibt, in dem
  nur die Resolutionsregel verwendet wird, und an dessen Blättern nur
  die Klauseln $K_1, \dots, K_m$ stehen.

  Die Aussage auf der vorhergehenden Folie bedeutet, dass die
  Ableitungsbeziehung $C_1 \derivesresfwd C_2$ korrekt ist: aus wahren
  Prämissen kann durch Resolution keine falsche Aussage gezeigt
  werden.
\end{frame}

\begin{frame}
  \frametitle{Resolution: Vollständigkeit}
  Mit dem ACL2-Beweiskalkül haben wir eine korrekte und vollständige
  Ableitungsregel kennengelernt:

  \begin{displaymath}
    \phi \models \psi \quad\text{genau dann, wenn}\quad \phi \derives \psi
  \end{displaymath}

  Jede in einer "`Wissensbasis"' $\phi$ wahre Formel $\psi$ kann also
  mit dem ACL2-Kalkül aus $\phi$ abgeleitet werden (und es können
  keine falschen Formeln abgeleitet werden).

  Gilt für den Resolutionskalkül eine ähnliche Eigenschaft?  Man kann
  sich, z.B.\ fragen, ob jede wahre Formel $C_2$ in CNF aus einer
  Wissensbasis $C_1$, die nur aus Klauseln besteht, abgeleitet werden
  kann:
  \begin{displaymath}
    \text{Frage:}\quad C_1 \models C_2 \quad\text{genau dann, wenn}\quad
    C_1 \derivesresfwd C_2
    \quad\text{?}
  \end{displaymath}

\end{frame}

\begin{frame}
  \frametitle{Resolution}
  Resolution ist \emph{nicht} vollständig: Sei $C$ eine Formel in CNF
  (also eine Menge von Klauseln) und $K$ eine Klausel.  Aus $C \models
  K$ folgt nicht $C \derivesresfwd K$.

  Beispiel: Da $A \lor \lnot A$ allgemeingültig ist (also $\models A
  \lor \lnot A$ wahr ist), gilt natürlich auch $B, \lnot C, D
  \models A \lor \lnot A$.  Nachdem in $\{B, \lnot C, D\}$ aber keine
  komplementären Literale vorkommen ist die Resolutionsregel gar nicht
  anwendbar, man kann also $A \lor \lnot A$ nicht ableiten.
\end{frame}

\begin{frame}
  \frametitle{Resolution}
  Resolution ist aber \emph{widerspruchsvollständig}: wenn $C$
  inkonsistent ist, dann lässt sich $\bot$ durch Resolution ableiten:
  \begin{displaymath}
    \text{Wenn}\quad C \models \bot \qtext{dann} C \derivesresfwd \bot
  \end{displaymath}

  Das ist aber schon genug, um jeden aussagenlogischen Schluss
  (zwischen Formeln in CNF) durch Resolution beweisen zu können:
  \begin{displaymath}
    C_1 \models C_2 \qtext{genau dann, wenn} C_1 \land \lnot C_2 \models \bot
  \end{displaymath}
  (Die linke Seite sagt, dass in jeder Belegung, in der $C_1$ wahr ist
  auch $C_2$ wahr ist.  Ist dass der Fall, so kann es keine Belegung
  geben, in der $C_1$ wahr ist und $C_2$ falsch; das ist genau die
  Aussage auf der rechten Seite.  Offensichtlich gilt auch die
  umgekehrte Richtung.)
\end{frame}

\begin{frame}
  \frametitle{Resolutionsabschluss}
  Zum Beweis der Widerspruchsvollständigkeit führen wir folgenden
  Begriff ein:
  \begin{definition}
    Sei $C$ eine Menge von Klauseln.  Der Resolutionsabschluss
    $\RC(C)$ (resolution closure) von $C$ ist der Fixpunkt der
    Iteration
    \begin{displaymath}
      C \leftarrow C \cup \Res(C),
    \end{displaymath}
    wobei $\Res(C)$ die Menge aller Klauseln ist, die durch Anwendung
    der Resolutionsregel auf zwei Klauseln aus $C$ entstehen.
  \end{definition}
  Da $C$ nur endlich viele Literale enthält und wir die Klauseln als
  Mengen betrachten ist $\RC(C)$ für jedes $C$ definiert. 
\end{frame}

\begin{frame}
  \frametitle{Widerspruchsvollständigkeit der Resolution}
  \begin{theorem}[Grundresolutionstheorem]
    Wenn eine Menge von Klauseln $C$ unerfüllbar ist, dann enthält der
    Resolutionsabschluss von $C$, $\RC(C)$ die leere Klausel.
  \end{theorem}
  Beweis: Falls $\RC(C)$ nicht die leere Klausel enthält können wir
  eine Belegung $\eta$ konstruieren, die $C$ erfüllt: Seien $L_1,
  \dots, L_k$ die Literale aus $C$; sei $\eta(L_j)$ für alle $j < i$
  definiert, dann definieren wir rekursiv $\eta(L_i)$ durch
  \begin{itemize}
  \item $\eta(L_j) = \false$, falls es eine Klausel in $C$ gibt, die
    $\lnot L_j$ enthält und deren andere Literale unter $\eta$ alle
    falsch sind.
  \item Sonst ist $\eta(L_j)$ wahr.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Widerspruchsvollständigkeit der Resolution}
  \begin{theorem}[Grundresolutionstheorem]
    Wenn eine Menge von Klauseln $C$ unerfüllbar ist, dann enthält der
    Resolutionsabschluss von $C$, $\RC(C)$ die leere Klausel.
  \end{theorem}
  Beweis (Fortsetzung): $\eta(C) = \true$.  Angenommen das wäre nicht
  der Fall, so gäbe es einen kleinsten Index $i$ für den eine Klausel
  $K$ aus $\RC(C)$ in der Konstruktion von $\eta$ den Wert $\false$
  erhielte. Dann hätte $K$ die Form $L_1, \dots, L_{i-1}, A_i$ oder $L_1,
  \dots, L_{i-1}, \lnot A_i$ mit $\eta(L_j) = \false$ für $j<i$.  Wäre
  nur eine der Klauseln in $\RC(C)$ so erhielte sie nach Konstruktion
  den Wert $\true$; es müssten also beide Klauseln in $\RC(C)$ sein.
  Dann wäre aber die Resolvente der beiden Klauseln ebenfalls in
  $\RC(C)$ und falsch, im Widerspruch zur Minimalität von $K$.\qed
\end{frame}

\begin{frame}
  \frametitle{Theorembeweisen durch Resolution}
  Beim Theorembeweisen durch Resolution geht man folgendermaßen vor:

  Gezeigt werden soll $\phi \models \psi$.
  \begin{itemize}
  \item Wandle $\phi \land \lnot \psi$ in CNF um
  \item Versuche daraus $\bot$ (die leere Klausel) durch Resolution
    abzuleiten
  \end{itemize}
  Gelingt das, ist $\phi \land \lnot \psi$ widersprüchlich, somit gilt
  $\phi \models \psi$.

  Gelingt das nicht ist $\phi \land \lnot \psi$ konsistent, $\phi
  \models \psi$ gilt damit nicht. Aus dem Beweis des
  Grundresolutionstheorems lässt sich eine Belegung $\eta$
  konstruieren, die $\phi$ und $\lnot \psi$ erfüllt.
\end{frame}


\begin{frame}
  \frametitle{Resolutionsbeweise}
  Wir schreiben $\phi \derivesres \psi$ wenn es einen
  Resolutionsbeweis gibt, der mit dem auf der vorhergehenden Folie
  beschriebenen Verfahren gefunden werden kann.  Es gilt also
  \begin{displaymath}
    \phi \derivesres \psi \quad\text{genau dann, wenn}\quad
    \cnf(\phi \land \lnot\psi) \derivesresfwd \bot
  \end{displaymath}
\end{frame}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
